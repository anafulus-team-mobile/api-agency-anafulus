<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

////////////////////////////////////// api for mobile-agent 
////////////// auth - Agen
$router->post('/mobile-agency/register', 'AuthController@register');
$router->get('/mobile-agency/login', 'AuthController@login');
// change password in profile
$router->put('/mobile-agency/change-password/{id_agency}', 'AuthController@changePassword');
$router->get('/mobile-agency/check-password/{id_agency}', 'AuthController@checkPassword');
$router->put('/mobile-agency/change-phoneNumber/{id_agency}', 'AuthController@changePhoneNumber');
$router->put('/mobile-agency/forget-password', 'AuthController@forgetPassword');

////////////// Dashboard
$router->get('/mobile-agency/view-promotion', 'HomeController@viewPromotion');
$router->get('/mobile-agency/view-kospin', 'HomeController@viewKospinLocation');
$router->get('/mobile-agency/view-helps/{id}','HomeController@viewHelp');
$router->get('/mobile-agency/view-helpsDetail/{id}','HomeController@viewHelpDetail');
$router->get('/mobile-agency/view-information/{name}','HomeController@viewInformation');
$router->get('/mobile-agency/filterByCategory', 'HomeController@filterByCategory');
$router->get('/mobile-agency/filterByType', 'HomeController@filterByType');
$router->get('/mobile-agency/filterByRate', 'HomeController@filterByRating');
$router->get('/mobile-agency/historyLoan/{id_agency}', 'HomeController@historyAddLoan');
$router->get('/mobile-agency/view-notificationAgency/{id_agency}','HomeController@viewNotificationAgency');
$router->get('/mobile-agency/detail-notificationTransaction/{id_transaction}','HomeController@detailNotifTransaction');
$router->delete('/mobile-agency/delete-notification/{id_notif}', 'HomeController@deleteNotification');
$router->put('/mobile-agency/read-notification/{id_notif}', 'HomeController@readNotification');
$router->get('/mobile-agency/search-borrowerApplicant/{id_agency}', 'HomeController@searchBorrowerApplicant');
$router->get('/mobile-agency/search-regulerLoan/{id_agency}', 'HomeController@searchRegularLoan');
$router->get('/mobile-agency/view-allAgencies', 'HomeController@getAllAgencies');
$router->get('/mobile-agency/view-master-headAgent', 'HomeController@getMasterHeadAgent');
$router->get('/mobile-agency/view-master-agent', 'HomeController@getMasterAgent');

// Head Agent 
$router->get('/mobile-agency/view-allAgents/{idSubdistrict}', 'HomeController@viewAllAgents');

// Agent
$router->get('/mobile-agency/view-regularLoan/{id}', 'HomeController@viewRegularLoan');
// $router->get('/mobile-agency/view-lateLoan/{id}', 'HomeController@viewLateLoan');
$router->get('/mobile-agency/view-loan-tanggungRenteng/{idAgency}', 'HomeController@viewLoanTanggungRenteng');
$router->get('/mobile-agency/view-detail-tanggungRenteng/{id}', 'HomeController@detailTanggungRenteng');
$router->get('/mobile-agency/view-loan-memberTanggungRenteng/{idLoan}', 'HomeController@viewMemberTanggungRenteng');
$router->get('/mobile-agency/view-borrowerApplicant/{id}', 'HomeController@viewBorrowerApplicant');
$router->get('/mobile-agency/view-headAgent/{idSubdistrict}', 'HomeController@viewHeadAgent');
$router->get('/mobile-agency/all-borrowers', 'HomeController@getAllBorrowers');


////////////////////////Agency Controller///////////////////////////
$router->post('/mobile-agency/update-photo-profile/{id}', 'AgencyController@updatePhotoProfile');
$router->put('/mobile-agency/update-personalData/{id}', 'AgencyController@updatePersonalData');
$router->get('/mobile-agency/view-profileAgency/{id}', 'AgencyController@viewProfileAgency');
$router->get('/mobile-agency/view-historyHead/{id}', 'AgencyController@viewHistoryHead');
$router->get('/mobile-agency/view-loansHead/{id}', 'AgencyController@viewLoanHead');
$router->get('/mobile-agency/view-bankAccountAgency/{id}', 'AgencyController@viewBankAccountAgency');
$router->post('/mobile-agency/create-bankAccountAgency/{id_agency}', 'AgencyController@createBankAccountAgency');
$router->put('/mobile-agency/update-bankAccountAgency/{id_agency}', 'AgencyController@updateBankAccountAgency');
$router->delete('/mobile-agency/delete-bankAccountAgency/{id_agency}', 'AgencyController@deleteBankAccountAgency');
$router->get('/mobile-agency/view-borrowerAgent/{idAgent}', 'AgencyController@viewBorrowerAgent');
// $router->get('/mobile-agency/search-borrowerLocalIdAgent/{id}', 'AgencyController@searchBorrowerLocalIdAgent');
$router->get('/mobile-agency/view-detailBorrowerApplicant/{id}', 'AgencyController@detailBorrowerApplicant');
$router->get('/mobile-agency/view-detailRegulerLoan/{id_agency}', 'AgencyController@detailRegularLoan');
// $router->get('/mobile-agency/view-detailLateLoan/{id_agency}', 'AgencyController@detailLateLoan');
$router->get('/mobile-agency/list-borrower/{id_sub_district}', 'AgencyController@listBorrowerAgency');
$router->get('/mobile-agency/view-detailBorrower/{id_borrower}', 'AgencyController@detailBorrowerAgency');
$router->put('/mobile-agency/withdraw/{id_agency}', 'AgencyController@withdraw');
// $router->get('/mobile-agency/all-agencies', 'AgencyController@getAllAgencies');


$router->get('/mobile-agency/list-provinces', 'AgencyController@viewProvince');
$router->get('/mobile-agency/list-regencies/{idProvince}', 'AgencyController@viewRegency');
$router->get('/mobile-agency/list-subdistricts/{idRegence}', 'AgencyController@viewSubdistrics');
$router->get('/mobile-agency/list-village/{idSubdistrict}', 'AgencyController@viewVillage');

/////////////////////// Agent Controller//////////////////////////
// $router->post('/mobile-agency/create-question/{id}', 'AgentController@createQuestion');
// $router->get('/mobile-agency/view-commisionBalance/{id}', 'AgentController@viewCommissionBalance');
// $router->put('/mobile-agency/update-personalData/{id}', 'AgentController@updatePersonalData');
$router->get('/mobile-agency/view-profile/{id}', 'AgentController@viewProfile');
$router->get('/mobile-agency/view-history/{id}', 'AgentController@viewHistory');
$router->get('/mobile-agency/view-loans/{id}', 'AgentController@viewLoan');
$router->put('/mobile-agency/update-bankAccount/{id}', 'AgentController@updateBankAccount');
// $router->get('/mobile-agency/view-borrowerProfile/{id}', 'AgentController@viewBorrowerProfile');

/////////////////////////// Loan Controller////////////////////
$router->post('/mobile-agency/loan/create-loan/{id}', 'LoanController@createLoan');
$router->post('/mobile-agency/loan/create-loanGroup/{idAgency}', 'LoanController@createLoanGroup');
$router->get('/mobile-agency/filterAllByStatus/{id}', 'LoanController@filterAllByStatus');
$router->put('/mobile-agency/confirm-loan/{id_loan}', 'LoanController@confirmLoan');
$router->put('/mobile-agency/reject-loan/{id_loan}', 'LoanController@rejectLoan');
$router->get('/mobile-agency/view-borrowerGroup/{id}', 'LoanController@viewBorrowerGroup');
$router->post('/mobile-agency/create-borrower/{id_agency}', 'LoanController@createDataBorrower');
$router->post('/mobile-agency/create-borrower-and-loan/{id_agency}', 'LoanController@createDataBorrowerAndLoan');

$router->put('/mobile-agency/update-dataBorrower/{id_borrower}', 'LoanController@updateDataBorrower');
$router->put('/mobile-agency/update-addressBorrower/{id_borrower}', 'LoanController@updateAddressBorrower');
$router->post('/mobile-agency/update-fileBorrower/{id_borrower}', 'LoanController@updateFileRequirement');
$router->get('/mobile-agency/view-profile-addLoan', 'LoanController@viewProfileAddLoan');
$router->get('/mobile-agency/search-borrowerLocalId\{id}', 'LoanController@searchBorrowerLocalId');
$router->get('/mobile-agency/filter-date-loanApplicant/{id_agency}', 'LoanController@filterDateBorrowerApplicant');
$router->get('/mobile-agency/filter-date-loanYetApproved/{id_agency}', 'LoanController@filterDateLoanYetApproved');
$router->get('/mobile-agency/filter-date-loanApproved/{id_agency}', 'LoanController@filterDateLoanApproved');
$router->get('/mobile-agency/filter-date-loanOnProgress/{id_agency}', 'LoanController@filterDateLoanOnProgress');
$router->get('/mobile-agency/filter-date-loanGroup/{id_agency}', 'LoanController@filterDateLoanGroup');
// $router->get('/mobile-agency/filter-date-lateLoan/{id_agency}', 'LoanController@filterDateLateLoan');
