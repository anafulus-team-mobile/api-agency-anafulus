<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class GroupLoans extends Model
{
	protected $table = 'group_loans';

    protected $fillable = [

    ];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loans', 'id', 'id_loan');
    }

    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrowers', 'id_borrower', 'id');
    }

    public function detailBorrower()
    {
        return $this->belongsTo('App\Models\DetailBorrowers', 'id_borrower', 'id_borrower');
    }

}

