<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class DetailBorrowers extends Model
{
	protected $table = 'd_borrowers';

    protected $fillable = [
        
    ];

    public function province()
    {
        return $this->hasOne('App\Models\Provinces','id_d_borrower','id');
    }

    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrowers', 'id_borrower', 'id');
    }
}

