<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Admins extends Model
{
	protected $table = 'm_admins';

    protected $fillable = [
        'name',
        'email',
        'role',
        'password'
    ];
}

