<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AgentNotifications extends Model
{
	protected $table = 'agent_notifications';

    protected $fillable = [

    ];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loans', 'id_loan', 'id');
    }
    public function borrower()
    {
        return $this->hasMany('App\Models\Borrowers', 'id_borrower', 'id');
    }
    public function agent()
    {
        return $this->belongsTo('App\Models\Agents', 'id_agent', 'id');
    }
    public function headAgent()
    {
        return $this->belongsTo('App\Models\HeadAgents', 'id_head_agent', 'id');
    }
}

