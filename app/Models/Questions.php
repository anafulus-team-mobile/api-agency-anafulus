<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Questions extends Model
{
	protected $table = 'questions';

    protected $fillable = [
        
    ];

    public function virtualAccount()
    {
        return $this->belongsTo('App\Models\VirtualAccounts');
    }

    public function help()
    {
        return $this->hasMany('App\Models\Helps', 'id_question', 'id');
    }
}

