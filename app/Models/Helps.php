<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Helps extends Model
{
	protected $table = 'helps';

    protected $fillable = [
        
    ];

    public function question()
    {
        return $this->belongsTo('App\Models\Questions', 'id_question', 'id');
    }
    public function virtualAccount()
    {
        return $this->belongsTo('App\Models\VirtualAccounts', 'id_help', 'id');
    }
}

