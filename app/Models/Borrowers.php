<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Borrowers extends Model
{
	protected $table = 'm_borrowers';

    protected $fillable = [
        'borrower_local_id',
        'borrower_family_card_id'

    ];

    public function detailBorrower()
    {   
        return $this->hasMany('App\Models\DetailBorrowers', 'id_borrower', 'id');
    }

    public function loan()
    {   
        return $this->hasMany('App\Models\Loans', 'id_borrower', 'id');
    }
    
}

