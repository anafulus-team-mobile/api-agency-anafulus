<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class DetailAgents extends Model
{
	protected $table = 'd_agents';

    protected $fillable = [
        
    ];

    public function agent()
    {
        return $this->belongsTo('App\Models\Agents', 'id_agent', 'id');
    }

    public function headAgent()
    {
        return $this->belongsTo('App\Models\HeadAgents', 'id_head_agent', 'id');
    }

    public function subDistrict()
    {
        return $this->belongsTo('App\Models\SubDistricts', 'id_sub_district', 'id');
    }

}

