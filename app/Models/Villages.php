<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Villages extends Model
{
	protected $table = 'villages';

    protected $fillable = [
        
    ];

    public function subDistrict()
    {
        return $this->belongsTo('App\Models\SubDistricts');
    }
}

