<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class VirtualAccounts extends Model
{
	protected $table = 'virtual_accounts';

    protected $fillable = [
        
    ];

    public function question()
    {
        return $this->hasMany('App\Models\Questions', 'id_virtual_account', 'id');
    }
    public function help()
    {
        return $this->hasMany('App\Models\Helps','id_virtual_account','id');
    }
}

