<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\DateLoan;
use DateTime;
use App\Models\BorrowerNotifications;
use App\Models\Installments;
use App\Models\Loans;
use App\Models\AdminFee;
use App\Models\DetailBorrowers;
use App\Models\Provinces;
use App\Models\Regencies;

// use Carbon\Carbon;

class BorrowerService {

    public function viewBorrower($id)
    {
        $borrower = DB::table('m_borrowers')
            ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'm_borrowers.id')
            ->select('d_borrowers.name', 'd_borrowers.email','d_borrowers.phone_number', 'm_borrowers.birth_date',
                    'm_borrowers.privyId_registeration_status','m_borrowers.id', 'm_borrowers.gender', 'd_borrowers.last_education', 'd_borrowers.last_job', 
                    'm_borrowers.borrower_local_id','d_borrowers.family_card_id', 'd_borrowers.family_phone_number', 
                    'd_borrowers.family_card_id', 'd_borrowers.principal_taxpayer_id', 'd_borrowers.family_relationship', 
                    'd_borrowers.domicile_address', 'd_borrowers.rating', 'd_borrowers.profil_image','d_borrowers.family_name',
                    'm_borrowers.is_active','d_borrowers.borrower_loan as loan_status','d_borrowers.id_selfie_image', 'd_borrowers.local_id_image', 
                    'd_borrowers.created_at', 'd_borrowers.home_image', 'd_borrowers.salary_slip_image')
            ->where('m_borrowers.id','=',$id)
            ->get();
        // if()
        // dd($borrower[0]);
        return $borrower;
    }

    public function viewProfileBorrower($id)
    {
        $borrower = DB::table('m_borrowers')
            ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'm_borrowers.id')
            ->where('m_borrowers.id','=',$id) //Id adalah integer
            ->first();

        $completeDataBorrower = DB::table('m_borrowers')
            ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'm_borrowers.id')
            ->join('villages', 'villages.id', '=', 'd_borrowers.id_village')
            ->join('sub_districts', 'sub_districts.id', '=', 'villages.id_sub_district')
            ->join('regencies', 'regencies.id', '=', 'sub_districts.id_regency')
            ->join('provinces', 'provinces.id', '=', 'regencies.id_province')
            ->where('m_borrowers.id','=',$id) //Id adalah integer
            ->first();
            if($borrower->id_village==null){
                return $borrower;
            } else {
                return $completeDataBorrower;
            }
    }

    public function viewBorrowerGroup($id)
    {
        $borrower = DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', '=', 'm_borrowers.id')
            ->join('villages', 'villages.id', '=', 'd_borrowers.id_village')
            ->join('sub_districts','sub_districts.id', '=', 'villages.id_sub_district')
            ->select('m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number',
                    'd_borrowers.id_card_address','d_borrowers.rating','loans.is_confirm_agent',
                    'loans.is_confirm_agent','loans.loan_status','loans.created_at'
                    ,'villages.id_sub_district as id_sub_district')
            ->where('villages.id_sub_district', $id)
            ->where(function ($query) {
                $query->orwhere('loans.is_confirm_first_admin','=','0')
                ->orwhere('loans.is_confirm_last_admin','=','0')
                ->orwhere('loans.loan_status','=','Pinjaman Dibatalkan')
                ->orwhere('loans.loan_status','=','Pinjaman Ditolak')
                ->orwhere('loans.loan_status','=','Pinjaman Selesai');
            })
            ->latest('loans.created_at')
            ->get();
        return $borrower;
    }

    public function viewBorrowerAgent($id)
    {
        $query= DB::table('loans')
        // ->join('m_agents','m_agents.id','=','loans.id_agent')
        // ->join('d_agents','d_agents.id_agent','=','m_agents.id')
        ->join('m_borrowers', 'm_borrowers.id', '=', 'loans.id_borrower')
        ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'loans.id_borrower')
        ->select( 'loans.id as id_loan', 'm_borrowers.borrower_local_id','d_borrowers.name'
                ,'d_borrowers.phone_number','d_borrowers.rating','d_borrowers.id_card_address'
                ,'loans.loan_status','loans.loan_principal'
                ,'loans.loan_approved', 'loans.tenor','loans.installment_nominal'
                ,'loans.loan_category','loans.loan_type','loans.created_at'
                ,'loans.is_confirm_agent','loans.is_confirm_first_admin'
                ,'loans.is_confirm_last_admin','loans.is_withdraw'
                )
        ->where('loans.id_agent', $id)
        ->whereNull('loans.group_name')
        ->get();
        return $query;
    }

    public function sumBorrowerAgent($id)
    {
        $query= DB::table('loans')
        ->join('m_agents','m_agents.id','=','loans.id_agent')
        ->join('d_agents','d_agents.id_agent','=','m_agents.id')
        ->join('group_loans','group_loans.id_loan','=','loans.id')
        ->join('m_borrowers', 'm_borrowers.id', '=', 'group_loans.id_borrower')
        ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'm_borrowers.id')
        ->select( 'loans.id as id_loan', 'm_borrowers.borrower_local_id','d_borrowers.name'
                ,'d_borrowers.phone_number','d_borrowers.id_card_address','loans.loan_status'
                , 'loans.loan_approved', 'loans.tenor','loans.installment_nominal'
                ,'group_loans.group_name','group_loans.total_borrower'
                ,'loans.loan_category','loans.loan_type','loans.created_at' )
        ->where('loans.id_agent', $id)
        ->count();
        return $query;
    }


    public function searchBorrowerAgent($id, $borrower_local_id)
    {
        $query= DB::table('loans')
        ->join('m_agents','m_agents.id','=','loans.id_agent')
        ->join('d_agents','d_agents.id_agent','=','m_agents.id')
        ->join('group_loans','group_loans.id_loan','=','loans.id')
        ->join('m_borrowers', 'm_borrowers.id', '=', 'group_loans.id_borrower')
        ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'm_borrowers.id')
        ->select( 'loans.id as id_loan', 'm_borrowers.borrower_local_id','d_borrowers.name'
                ,'d_borrowers.phone_number','d_borrowers.id_card_address','loans.loan_status'
                , 'loans.loan_approved', 'loans.tenor','loans.installment_nominal'
                ,'group_loans.group_name','group_loans.total_borrower'
                ,'loans.loan_category','loans.loan_type','loans.created_at' )
        ->where('loans.id_agent', $id)
        ->where('m_borrowers.borrower_local_id', '=', $borrower_local_id)
        ->first();
        return $query;
    }

    public function listBorrowerAgent($id_sub_district){
        $list= DB::table('m_borrowers')
                // ->join('loans','loans.id_agent','m_agents.id')
                // ->join('d_agents','d_agents.id_agent','m_agents.id')
                // ->join('sub_districts','sub_districts.id','d_agents.id_sub_district')
                ->join('d_borrowers','d_borrowers.id_borrower','m_borrowers.id')
                ->join('villages','villages.id','d_borrowers.id_village')
                ->join('sub_districts','sub_districts.id','villages.id_sub_district')
                ->select('sub_districts.sub_district_name'
                        ,'m_borrowers.id as id_borrower','m_borrowers.borrower_local_id'
                        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
                        ,'d_borrowers.phone_number','d_borrowers.domicile_address'
                        ,'d_borrowers.id_card_address'                        
                        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
                        ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
                        ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
                        ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
                        ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
                        ,'d_borrowers.home_image','d_borrowers.salary_slip_image','d_borrowers.borrower_loan'
                        // ,'loans.loan_status'
                        )
                ->where('villages.id_sub_district', $id_sub_district)
                ->where(function ($query) {
                    $query->orwhere('d_borrowers.borrower_loan', '=', 'Belum Pernah')
                          ->orwhere('d_borrowers.borrower_loan', '=', 'Pinjaman Selesai');
                })
                ->get();
      
        return $list;
    }

    public function detailBorrower($id_borrower){
        $detail= DB::table('m_borrowers')
                ->join('d_borrowers','d_borrowers.id_borrower','m_borrowers.id')
                ->select(
                        'm_borrowers.id as id_borrower','m_borrowers.borrower_local_id'
                        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
                        ,'d_borrowers.phone_number','d_borrowers.domicile_address'
                        ,'d_borrowers.id_card_address'                        
                        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
                        ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
                        ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
                        ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
                        ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
                        ,'d_borrowers.home_image','d_borrowers.salary_slip_image','d_borrowers.borrower_loan'
                        )
                ->where('m_borrowers.id', $id_borrower)
                ->first();
        return $detail;
    }

    public function detailLoan($idLoan){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.loan_approved'
        ,'loans.tenor','loans.installment_nominal','loans.loan_status'
        ,'loans.loan_category'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        )
        ->where('loans.id', $idLoan)
        ->get();

        foreach($regulerLoan as $item){
            $installment= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNotNull('installments.payment_date')
            ->where('loans.id', $item->id_loan)
            ->count();

            // cicilan telah dibayar
            $item->paid_off_installment=$installment;
            $list[]= $item;
        }
            // dd($regulerLoan);
        return $regulerLoan;
    }

    public function installmentLoan($idLoan){
         // total cicilan
         $totalInstallment=DB::table('loans')
         ->join('installments','installments.id_loan','loans.id')
         ->select('loans.id as id_loan', 'loans.loan_status','loans.loan_principal'
            ,'loans.loan_approved','loans.loan_category','loans.loan_type','loans.tenor'
            ,'loans.installment_nominal','loans.installment_type', 'installments.id as id_installment'
            ,'installments.due_date')
         ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
         ->whereNull('loans.group_name')
         ->where('loans.id', $idLoan)
         ->get();

        return $totalInstallment;
    }

    public function installmentLateLoan($idLoan){
        // cicilan yang telat jatuh tempo
        $date = new Carbon;
        $lateInstallment= DB::table('loans')
        ->join('installments','installments.id_loan','loans.id')
        ->select('loans.id as id_loan','loans.installment_nominal','installments.due_date')
        ->where('loans.id',$idLoan)
        // ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
        // ->whereNull('loans.group_name')
        ->whereNull('installments.payment_date')
        ->where('installments.due_date', '<', $date)
        ->get();

       return $lateInstallment;
   }

  public function withdrawLoan($id_borrower, $balance, $tenor, $totalAdminFee, $adminFeePercentage){
        $borrower= DetailBorrowers::where('id_borrower',$id_borrower)->first();
        $borrower->finance_balance = $balance;
        $borrower->saveOrFail();
        
        $loan= Loans::where('id_borrower', $id_borrower)
        ->where('loan_status', '=', 'Pinjaman Disetujui')
        ->first();
        $loan->is_withdraw= 1;
        $loan->loan_status= "Cicilan Sedang Berjalan";
        $loan->loan_status_desc= "Anda Telah Menarik Dana dan Cicilan Telah Berjalan";
        $loan->saveOrFail();

        $dt = new DateTime();
       $dateLoan= DateLoan::where('id_loan', $loan->id)->first();
       $dateLoan->withdraw_fund_date = $dt->format('Y-m-d H:i:s');       
       $dateLoan->saveOrFail();

        $newNotif= new BorrowerNotifications();
        $newNotif->id_loan = $loan->id;
        $newNotif->id_borrower = $id_borrower;
        if($borrower->id_agent == null){
            $newNotif->id_head_agent = $borrower->id_head_agent;        
        }else if($borrower->id_head_agent == null){
            $newNotif->id_agent = $borrower->id_agent;        
        }
        $newNotif->description = 'Melakukan Tarik Dana Pinjaman ke Saldo';
        $newNotif->detail = 'Anda telah berhasil melakukan penarikan dana Pinjaman. jumlah saldo Anda telah bertambah sesuai dengan jumlah dana yang Anda tarik.';
        $newNotif->saveOrFail();

        $date = new Carbon;
        for($i=1; $i<=$tenor; $i++){
            $installment= new Installments();
            $installment->id_loan= $loan->id;
            $installment->id_borrower= $id_borrower;
            $installment->due_date= $date->addWeeks(1);
            $installment->saveOrFail();
        }

        $admin_fee= new AdminFee();
        $admin_fee->id_loan = $loan->id;
        $admin_fee->total_admin_fee = $totalAdminFee;
        $admin_fee->admin_fee_percentage = $adminFeePercentage;
        $admin_fee->description = "Dana Pinjaman Ditarik dan dikenai biaya Administrasi";
        $admin_fee->is_charged = 1;
        $admin_fee->saveOrFail();

        return "Tarik Dana Berhasil";
   }

   public function countBorrowerProvince(){
    $provinces= Provinces::get();
    
    foreach($provinces as $item){
        $borrower= DB::table('d_borrowers')
        ->join('villages','villages.id','d_borrowers.id_village')
        ->join('sub_districts','sub_districts.id','villages.id_sub_district')
        ->join('regencies','regencies.id','sub_districts.id_regency')
        ->join('provinces','provinces.id','regencies.id_province')
        // ->select('provinces.id')
        ->where('provinces.id', $item->id)
        // ->groupBy('id')
        ->count();
        

        $item->total_borrower= $borrower;

        $list[]= $item;
    }    
    $desc = collect($list)->sortBy('total_borrower')->reverse()->toArray();
    return array_slice($desc, 0,1, true); 
   }

   public function countBorrowerRegency(){
    $provinces= Regencies::get();
    
    foreach($provinces as $item){
        $borrower= DB::table('d_borrowers')
        ->join('villages','villages.id','d_borrowers.id_village')
        ->join('sub_districts','sub_districts.id','villages.id_sub_district')
        ->join('regencies','regencies.id','sub_districts.id_regency')
        // ->join('provinces','provinces.id','regencies.id_province')
        // ->select('provinces.id')
        ->where('regencies.id', $item->id)
        // ->groupBy('id')
        ->count();
        

        $item->total_borrower= $borrower;

        $list[]= $item;
    }    
    $desc = collect($list)->sortBy('total_borrower')->reverse()->toArray();
    
    return array_slice($desc, 0,1, true); 
   }

   public function countinstallmentLate(){

    $date = new Carbon;
    $notYetPaid= DB::table('loans')
    ->join('installments','installments.id_loan','loans.id')
    ->select('loans.id', 'installments.id as id_installment','loans.installment_nominal'
            ,'loans.loan_approved','installments.due_date', 'installments.payment_date'
    )
    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
    ->whereNull('installments.payment_date')
    ->where('installments.due_date', '<', $date)
    ->get(); //return id_loan

    $loanProgress= DB::table('loans')
    ->select('loan_approved')
    ->where('loan_status','=','Cicilan Sedang Berjalan')
    ->get();

    $loanDone= DB::table('loans')
    ->select('loan_approved')
    ->where('loan_status','=','Pinjaman Selesai')
    ->get();

    foreach($loanProgress as $loanProgg){
        $totalLoanProggress[] = $loanProgg->loan_approved;
    }   

    foreach($loanDone as $loanDon){
        $totalLoanDone[] = $loanDon->loan_approved;
    }   
    $totalAllLoan = array_sum($totalLoanDone) + array_sum($totalLoanProggress);

    foreach($notYetPaid as $item){
        $diff= date_diff(date_create($date), date_create($item->due_date))->format("%a");
        if($diff > 90){
            $list[]= $item->installment_nominal;
        }
    }

    $totalInstallmentNominal = array_sum($list);

    $havePaid = DB::table('loans')
    ->join('installments','installments.id_loan','loans.id')
    ->select('loans.id', 'installments.id as id_installment','loans.installment_nominal'
    , 'installments.due_date', 'installments.payment_date'
    )
    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
    ->get();

    foreach($havePaid as $a){
        if($a->due_date < $a->payment_date){
            if(date_diff(date_create($a->payment_date), date_create($a->due_date))->format("%a") > 90){
                $b[]=$a->installment_nominal;
            }
        } else {
            $b[] = 0;
        }
    }
    $totalhavePaid= array_sum($b);


    $result = (object) ['totalAllLoan' => $totalAllLoan, 'totalhavePaid' => $totalhavePaid, 'totalInstallmentNominal' => $totalInstallmentNominal];

    return $result;
   }

   public function detailNotificationStatus($idnotif){
    $detail= DB::table('borrower_notifications')
                ->join('loans','loans.id','borrower_notifications.id_loan')
                ->select('borrower_notifications.id','borrower_notifications.detail'
                ,'borrower_notifications.description','borrower_notifications.is_read'
                ,'borrower_notifications.id_head_agent','borrower_notifications.id_agent'
                ,'borrower_notifications.id_borrower','borrower_notifications.id_loan'
                ,'borrower_notifications.id_borrower_withdraw_transaction','loans.loan_status')
                ->where('borrower_notifications.id', $idnotif)
                ->first();

    return $detail;
   }

   public function detailNotificationWithdraw($idnotif){
    $detail= DB::table('borrower_notifications')
                ->join('borrower_withdraw_transactions','borrower_withdraw_transactions.id','borrower_notifications.id_borrower_withdraw_transaction')
                ->select('borrower_notifications.id','borrower_notifications.detail'
                ,'borrower_notifications.description','borrower_notifications.is_read'
                ,'borrower_notifications.id_head_agent','borrower_notifications.id_agent'
                ,'borrower_notifications.id_borrower','borrower_notifications.id_loan'
                ,'borrower_notifications.id_borrower_withdraw_transaction'
                ,'borrower_withdraw_transactions.id_virtual_account'
                ,'borrower_withdraw_transactions.withdraw_nominal'
                ,'borrower_withdraw_transactions.withdraw_category')
                ->where('borrower_notifications.id', $idnotif)
                ->first();

    return $detail;
   }

   public function detailNotificationPayment($idnotif){
    $detail= DB::table('borrower_notifications')
                ->join('payment_histories','payment_histories.id','borrower_notifications.id_payment_history')
                ->select('borrower_notifications.id','borrower_notifications.detail'
                ,'borrower_notifications.description','borrower_notifications.is_read'
                ,'borrower_notifications.id_head_agent','borrower_notifications.id_agent'
                ,'borrower_notifications.id_borrower','borrower_notifications.id_loan'
                ,'borrower_notifications.id_borrower_withdraw_transaction'
                ,'payment_histories.id_installment'
                ,'payment_histories.id_virtuall_account'
                ,'payment_histories.virtual_account_number'
                ,'payment_histories.is_payment','payment_histories.category'
                ,'payment_histories.payment_type','payment_histories.total_payment'
                ,'payment_histories.installment_paid_off'
                ,'payment_histories.payment_due_date'
                // ,'payment_histories.is_payment','payment_due_date'
                )
                ->where('borrower_notifications.id', $idnotif)
                ->first();

    return $detail;
   }

}