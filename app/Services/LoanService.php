<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Loans;
use App\Models\BorrowerNotifications;
use App\Models\AdminNotifications;
use App\Models\DateLoan;
use App\Models\DetailBorrowers;
use App\Models\Installments;
use App\Models\AdminFee;

use DateTime;

class LoanService {

    public function viewLoan($id)
    {
        $loan = DB::table('loans')->where('loans.id','=',$id)->first();
        if($loan->id_head_agent == null){
            $query = DB::table('loans')
            ->join('d_agents', 'd_agents.id_agent', '=', 'loans.id_agent')
            ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'loans.id_borrower')
            ->join('m_borrowers', 'm_borrowers.id', '=', 'd_borrowers.id_borrower')
            ->join('villages', 'villages.id', '=', 'd_borrowers.id_village')
            ->join('sub_districts', 'sub_districts.id', '=', 'villages.id_sub_district')
            ->join('regencies', 'regencies.id', '=', 'sub_districts.id_regency')
            ->join('provinces', 'provinces.id', '=', 'regencies.id_province')
            ->join('date_loan', 'date_loan.id_loan', '=', 'loans.id')
            ->select('d_borrowers.name as name_borrower', 'd_borrowers.email as email_borrower', 
                     'd_borrowers.phone_number as phone_number_borrower', 'm_borrowers.gender as gender_borrower','m_borrowers.birth_date as birth_date_borrower',
                     'd_borrowers.last_education as education_borrower', 'm_borrowers.borrower_local_id as ktp_borrower', 'd_borrowers.family_card_id as family_card_id_borrower',
                     'd_borrowers.income as income_borrower', 'd_borrowers.last_job as last_job_borrowerr', 'd_borrowers.id_card_address as borrower_address',
                     'loans.total_paid_off_lender', 'd_borrowers.local_id_image', 'd_borrowers.id_selfie_image', 'd_borrowers.home_image', 'd_borrowers.salary_slip_image', 'd_borrowers.principal_taxpayer_id',
                     'd_agents.name as name_agent','d_agents.email as email_agent', 'd_agents.phone_number as phone_number_agent', 'd_agents.agent_code',
                     'd_agents.id_card_address as address_agent', 'sub_districts.sub_district_name as district_agent', 'regencies.regency_name as regency_agent', 'provinces.province_name as provincy_agent', 
                     'd_borrowers.id_borrower','loans.tenor','loans.loan_category', 'date_loan.submition_date as submission_date', 'loans.loan_purpose',
                     'loans.loan_type', 'loans.loan_principal', 'loans.loan_approved', 'loans.loan_status', 'loans.installment_nominal', 'loans.installment_type', 
                     'loans.loan_status_desc', 'loans.is_confirm_agent', 'loans.is_confirm_first_admin', 'loans.is_confirm_last_admin')
            ->where('loans.id','=',$id) //Id adalah integer
            ->get();
        } else{
            $query = DB::table('loans')
            ->join('d_agents', 'd_agents.id_head_agent', '=', 'loans.id_head_agent')
            ->join('d_borrowers', 'd_borrowers.id_borrower', '=', 'loans.id_borrower')
            ->join('m_borrowers', 'm_borrowers.id', '=', 'd_borrowers.id_borrower')
            ->join('villages', 'villages.id', '=', 'd_borrowers.id_village')
            ->join('sub_districts', 'sub_districts.id', '=', 'villages.id_sub_district')
            ->join('regencies', 'regencies.id', '=', 'sub_districts.id_regency')
            ->join('provinces', 'provinces.id', '=', 'regencies.id_province')
            ->join('date_loan', 'date_loan.id_loan', '=', 'loans.id')
            // ->join('aggreement_documentations', 'aggreement_documentations.id_loan', '=', 'loans.id')
            ->select('d_borrowers.name as name_borrower', 'd_borrowers.email as email_borrower', 
                     'd_borrowers.phone_number as phone_number_borrower', 'm_borrowers.gender as gender_borrower','m_borrowers.birth_date as birth_date_borrower',
                     'd_borrowers.last_education as education_borrower', 'm_borrowers.borrower_local_id as ktp_borrower', 'd_borrowers.family_card_id as family_card_id_borrower',
                     'd_borrowers.income as income_borrower', 'd_borrowers.last_job as last_job_borrowerr', 'd_borrowers.id_card_address as borrower_address',
                     'loans.total_paid_off_lender', 'd_borrowers.local_id_image', 'd_borrowers.id_selfie_image', 'd_borrowers.home_image', 'd_borrowers.salary_slip_image', 'd_borrowers.principal_taxpayer_id',
                     'd_agents.name as name_agent','d_agents.email as email_agent', 'd_agents.phone_number as phone_number_agent', 'd_agents.agent_code',
                     'd_agents.id_card_address as address_agent', 'sub_districts.sub_district_name as district_agent', 'regencies.regency_name as regency_agent', 'provinces.province_name as provincy_agent', 
                     'd_borrowers.id_borrower','loans.tenor','loans.loan_category', 'date_loan.submition_date as submission_date', 'loans.loan_purpose',
                     'loans.loan_type', 'loans.loan_principal', 'loans.loan_approved', 'loans.loan_status', 'loans.installment_nominal', 'loans.installment_type', 
                     'loans.loan_status_desc', 'loans.is_confirm_agent', 'loans.is_confirm_first_admin', 'loans.is_confirm_last_admin')
                    //  ,'aggreement_documentations.is_signed_borrower', 'aggreement_documentations.is_signed_admin')
            ->where('loans.id','=',$id) //Id adalah integer
            ->get();    
        }
        return $query;
    }

    public function viewAggrementDocumentationStatus($id)
    {
        $loan = DB::table('loans')->where('loans.id','=',$id)->first();
        if($loan->id_head_agent == null){
            $query = DB::table('loans')
            ->join('aggreement_documentations', 'aggreement_documentations.id_loan', '=', 'loans.id')
            ->select('aggreement_documentations.is_signed_borrower', 'aggreement_documentations.is_signed_admin')
            ->where('loans.id','=',$id) //Id adalah integer
            ->get();
        } else{
            $query = DB::table('loans')
            ->join('aggreement_documentations', 'aggreement_documentations.id_loan', '=', 'loans.id')
            ->select('aggreement_documentations.is_signed_borrower', 'aggreement_documentations.is_signed_admin')
            ->where('loans.id','=',$id) //Id adalah integer
            ->get();    
        }
        return $query;
    }

    public function viewInstallment($id)
    {
        $query = DB::table('installments')
        ->join('loans', 'loans.id', '=', 'installments.id_loan')
        ->where('loans.id','=',$id)
        ->paginate();
        return $query;
    }
    public function viewFundingLoans($id)
    {
        $dataFundingLoans = array();
        $query = DB::table('loans')
            ->where('loans.id','=',$id)
            ->first();
        if($query->list_id_lender){
            $allLenders = DB::table('m_lenders')
                ->join('d_lenders', 'd_lenders.id_lender', '=', 'm_lenders.id')
                ->get();
            foreach(json_decode($query->list_id_lender) as $idLender){
                foreach($allLenders as $lender){
                    if($idLender == $lender->id_lender){
                        $idMarginComission = DB::table('margin_commissions')
                            ->select("id")
                            ->where('id_loan','=',$id)
                            ->first();
                        $fundingLoan = DB::table('funding_loans')
                            ->select("funding_amount", "id_lender", "id_margin_commission")
                            ->where('id_lender','=',$lender->id_lender)
                            ->where('id_margin_commission','=',$idMarginComission->id)
                            ->first();
                        $privyId = DB::table('privyid_accounts')
                            ->select("privyId", "id_lender")
                            ->where('id_lender','=',$lender->id_lender)
                            ->first();

                        $paymentTransactions = DB::table('lender_payment_transactions')
                            ->where('id_lender','=',$lender->id_lender)
                            ->get();
                        $listIdLoans = array();
                        foreach($paymentTransactions as $paymentTransaction){
                            foreach(json_decode($paymentTransaction->list_id_loan) as $item){
                                if($item->id_loan == $id){
                                    $lender->payment_date =$paymentTransaction->payment_successful_date; 
                                    $lender->payment_type =$paymentTransaction->payment_type; 
                                    $lender->payment_status =$paymentTransaction->have_paid_off; 
                                }
                                array_push($listIdLoans, $item->id_loan);
                            }                                
                        }

                        $lender->funding_amount = $fundingLoan->funding_amount;
                        $lender->privyId = $privyId->privyId;

                        array_push($dataFundingLoans, $lender);
                    }
                }
            }
            return $dataFundingLoans;
        } else {
            return $dataFundingLoans;
        }
    }

    public function viewGroupLoan($id)
    {
        $query= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower','=','m_borrowers.id')
        ->join('date_loan','date_loan.id_loan','=','loans.id')
        ->select('date_loan.submition_date', 'date_loan.validation_date', 'date_loan.verification_date','date_loan.approvement_date','date_loan.withdraw_fund_date'
                ,'date_loan.cancellation_date','date_loan.rejection_date'
                ,'loans.id as id_loan','loans.installment_nominal','loans.tenor','loans.installment_type','loans.loan_category','loans.group_name','loans.id_borrower', 'loans.loan_status' , 'loans.loan_principal'
                ,'loans.total_borrower as total_member','loans.is_confirm_agent', 'loans.is_confirm_first_admin', 'loans.is_confirm_last_admin', 'loans.is_withdraw'
                ,'m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number', 'd_borrowers.rating'
                ,'d_borrowers.id_card_address'
                )
        ->where('loans.id_agent', $id)
        ->whereNotNull('loans.group_name')
        ->get();

        return $query;
    }

    public function viewGroupLoanHead($id)
    {
        $query= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower','=','m_borrowers.id')
        ->join('date_loan','date_loan.id_loan','=','loans.id')
        ->select('date_loan.submition_date', 'date_loan.validation_date', 'date_loan.verification_date','date_loan.approvement_date','date_loan.withdraw_fund_date'
                ,'date_loan.cancellation_date','date_loan.rejection_date'                
                ,'loans.id as id_loan','loans.installment_nominal','loans.tenor','loans.installment_type','loans.loan_category','loans.group_name','loans.id_borrower', 'loans.loan_status' , 'loans.loan_principal'
                ,'loans.total_borrower as total_member','loans.is_confirm_agent', 'loans.is_confirm_first_admin', 'loans.is_confirm_last_admin', 'loans.is_withdraw'
                ,'m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number', 'd_borrowers.rating'
                ,'d_borrowers.id_card_address'
                )
        ->where('loans.id_head_agent', $id)
        ->whereNotNull('loans.group_name')
        ->get();
        
        return $query;
    }

    public function detailGroupLoan($id, $id_group_loan)
    {
        $query= DB::table('loans')
        // ->join('date_loan','date_loan.id_loan','=','loans.id')
        ->join('group_loans','group_loans.id','=','loans.id_group_loan')
        ->join('m_borrowers','m_borrowers.id','=','group_loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower','=','m_borrowers.id')
        ->select('loans.id as id_loan','loans.id_agent','loans.tenor','loans.loan_category'
                , 'loans.loan_purpose','loans.loan_type', 'loans.loan_principal'
                , 'loans.loan_approved', 'loans.loan_status', 'loans.installment_nominal'
                , 'loans.installment_type', 'loans.id_group_loan'
                ,'group_loans.id_borrower as id_leader','loans.id_borrower as id_borrower'
                ,'m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number'
                ,'d_borrowers.id_card_address','d_borrowers.rating')
        ->where('loans.id_agent', $id)
        ->where('group_loans.id',$id_group_loan)
        ->get();
        return $query;

    }
    
    public function detailGroupLoanHead($id, $id_group_loan)
    {
        $query= DB::table('loans')
        // ->join('date_loan','date_loan.id_loan','=','loans.id')
        ->join('group_loans','group_loans.id','=','loans.id_group_loan')
        ->join('m_borrowers','m_borrowers.id','=','group_loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower','=','m_borrowers.id')
        ->select('loans.id as id_loan','loans.id_agent','loans.tenor','loans.loan_category'
                , 'loans.loan_purpose','loans.loan_type', 'loans.loan_principal'
                , 'loans.loan_approved', 'loans.loan_status', 'loans.installment_nominal'
                , 'loans.installment_type', 'loans.id_group_loan'
                ,'group_loans.id_borrower as id_leader','loans.id_borrower as id_borrower'
                ,'m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number'
                ,'d_borrowers.id_card_address','d_borrowers.rating')
        ->where('loans.id_head_agent', $id)
        ->where('group_loans.id',$id_group_loan)
        ->get();
        return $query;
    }

    public function sumLateLoan($id)
    {
        $date = new Carbon;
        $installments= DB::table('loans')
        ->join('installments','installments.id_loan','loans.id')
        ->select('loans.id')
        ->where('loans.id_agent',$id)
        ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
        ->whereNull('loans.group_name')
        ->whereNull('installments.payment_date')
        ->where('installments.due_date', '<', $date)
        ->groupBy('id')
        ->get(); //return id_loan

        if($installments->isEmpty()){
            $listLate = 0;
            return $listLate;
        } else {
            foreach($installments as $id_loan){
                // dd($id_loan->id);
                $query= DB::table('loans')
                ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
                ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
                ->join('date_loan','date_loan.id_loan', 'loans.id')
                ->select('loans.id as id_loan','loans.id_agent as id_agent'
                ,'m_borrowers.id as id_borrower'
                ,'loans.loan_type','loans.loan_principal','loans.tenor'
                ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
                ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
                ,'d_borrowers.phone_number','d_borrowers.domicile_address'
                ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
                ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
                ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
                ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
                ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
                ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
                ,'date_loan.submition_date'
                )
                ->where('loans.id_agent',$id)
                ->whereNull('loans.group_name')
                ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                ->where('loans.id',$id_loan->id)
                ->get();    
                $finalData[] = $query;
            }
            foreach($finalData as $data1){
                foreach($data1 as $data2){
                    $listLate[] = $data2;
                }
            }
            return count($listLate);
        }
    }
    
    public function sumLateLoanHead($id)
    {
        $date = new Carbon;
        $installments= DB::table('loans')
        ->join('installments','installments.id_loan','loans.id')
        ->select('loans.id')
        ->where('loans.id_head_agent',$id)
        ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
        ->whereNull('loans.group_name')
        ->whereNull('installments.payment_date')
        ->where('installments.due_date', '<', $date)
        ->groupBy('id')
        ->get(); //return id_loan

        foreach($installments as $id_loan){
            // dd($id_loan->id);
            $query= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->join('date_loan','date_loan.id_loan', 'loans.id')
            ->select('loans.id as id_loan','loans.id_agent as id_agent'
            ,'m_borrowers.id as id_borrower'
            ,'loans.loan_type','loans.loan_principal','loans.tenor'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->where('loans.id_head-agent',$id)
            ->whereNull('loans.group_name')
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->where('loans.id',$id_loan->id)
            ->get();    
            $finalData[] = $query;
        }
        foreach($finalData as $data1){
            foreach($data1 as $data2){
                $listLate[] = $data2;
            }
        }
        return count($listLate);
    }

    public function viewLateLoan($id)
    {
        $date = new Carbon;
        $installments= DB::table('loans')
        ->join('installments','installments.id_loan','loans.id')
        ->select('loans.id')
        ->where('loans.id_agent',$id)
        ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
        ->whereNull('loans.group_name')
        ->whereNull('installments.payment_date')
        ->where('installments.due_date', '<', $date)
        ->groupBy('id')
        ->get(); //return id_loan

        foreach($installments as $id_loan){
            $query= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->join('date_loan','date_loan.id_loan', 'loans.id')
            ->select('loans.id as id_loan','loans.id_agent as id_agent'
            ,'m_borrowers.id as id_borrower'
            ,'loans.loan_type','loans.loan_principal','loans.tenor'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
            ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
            )
            ->where('loans.id_agent',$id)
            ->whereNull('loans.group_name')
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->where('loans.id',$id_loan->id)
            ->get();    
            $finalData[] = $query;
        }
        foreach($finalData as $data1){
            foreach($data1 as $data2){
                $listLate[] = $data2;
            }
        }

        foreach($listLate as $a){
            // cicilan yang telat jatuh tempo
            $payment= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->select('loans.id as id_loan','loans.installment_nominal','installments.due_date')
            ->where('loans.id_agent',$id)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNull('installments.payment_date')
            ->where('installments.due_date', '<', $date)
            ->where('loans.id',$a->id_loan)
            ->count();

            // jmlh cicilan yg sudah dibayar 
            $countInstallment= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->select('loans.id as id_loan','loans.installment_nominal','installments.due_date')
            ->where('loans.id_agent',$id)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNotNull('installments.payment_date')
            ->where('loans.id',$a->id_loan)
            ->count();
            
            $due_date= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->select('installments.due_date')
            ->where('loans.id_agent', $id)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNull('installments.payment_date')
            ->where('loans.id', $a->id_loan)
            ->first();

            $a->total_late_installment= $payment;
            $a->count_installment= $countInstallment;
            $a->due_date= $due_date->due_date;
            $list[] = $a;
        }    
        // dd($listLate);

        return $listLate;
    }

    public function viewLateLoanHead($id)
    {
        $date = new Carbon;
        $installments= DB::table('loans')
        ->join('installments','installments.id_loan','loans.id')
        ->select('loans.id')
        ->where('loans.id_head_agent',$id)
        ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
        ->whereNull('loans.group_name')
        ->whereNull('installments.payment_date')
        ->where('installments.due_date', '<', $date)
        ->groupBy('id')
        ->get(); //return id_loan

        foreach($installments as $id_loan){
            $query= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->join('date_loan','date_loan.id_loan', 'loans.id')
            ->select('loans.id as id_loan','loans.id_agent as id_agent'
            ,'m_borrowers.id as id_borrower'
            ,'loans.loan_type','loans.loan_principal','loans.tenor'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
            ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
            )
            ->where('loans.id_head_agent',$id)
            ->whereNull('loans.group_name')
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->where('loans.id',$id_loan->id)
            ->get();    
            $finalData[] = $query;
        }
        foreach($finalData as $data1){
            foreach($data1 as $data2){
                $listLate[] = $data2;
            }
        }

        foreach($listLate as $a){
            $payment= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->select('loans.id as id_loan','loans.installment_nominal','installments.due_date')
            ->where('loans.id_head_agent',$id)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNull('installments.payment_date')
            ->where('installments.due_date', '<', $date)
            ->where('loans.id',$a->id_loan)
            ->count();
            $a->total_late_installment= $payment;
            $list[] = $a;
        }    

        return $listLate;
    }

    public function searchLocalId($id, $borrower_local_id)
    {
        $query= DB::table('loans')
        // ->join('date_loan','date_loan.id_loan','=','loans.id')
        ->join('group_loans','group_loans.id_loan','=','loans.id')
        ->join('m_borrowers','m_borrowers.id','=','group_loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower','=','m_borrowers.id')
        ->join('villages','villages.id','=','d_borrowers.id_village')
        ->join('sub_districts','sub_districts.id','villages.id_sub_district')
        ->select('loans.id as id_loan','loans.id_agent','loans.tenor','loans.loan_category'
                , 'loans.loan_purpose','loans.loan_type', 'loans.loan_principal'
                , 'loans.loan_approved', 'loans.loan_status', 'loans.installment_nominal'
                , 'loans.installment_type', 'loans.group_name'
                ,'group_loans.id_borrower','group_loans.total_borrower','m_borrowers.id as id_borrower'
                ,'m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number'
                ,'d_borrowers.id_card_address','d_borrowers.rating','d_borrowers.id_village'
                ,'villages.id_sub_district')
        ->where('villages.id_sub_district', $id)
        ->where('m_borrowers.borrower_local_id',$borrower_local_id)
        ->where(function ($query) {
            $query->orwhere('loan_status', '=', 'Belum Disetujui')
                  ->orwhere('loan_status', '=', 'Cicilan Sedang Berjalan')
                  ->orwhere('loan_status', '=', 'Pinjaman Disetujui');
        })
        ->first();
        return $query;
    }

    public function confirmLoan($id_loan, $id_agency, $status_agency){

        $confirmLoan = Loans::where('id', $id_loan)
                        ->first();
            $confirmLoan->is_confirm_agent = 1;
            $confirmLoan->loan_status_desc= "Pinjaman Telah Dikonfirmasi Oleh Agen";
            $confirmLoan->saveOrFail();

            $borrowerNotification= new BorrowerNotifications();
            $borrowerNotification->id_loan = $id_loan;
            $borrowerNotification->id_borrower= $confirmLoan->id_borrower;
            if($status_agency == 'Head Agent'){
                $borrowerNotification->id_head_agent = $id_agency;
            } else{
                $borrowerNotification->id_agent = $id_agency;
            }
            $borrowerNotification->description = 'Pinjaman dalam tahap Validasi Data';
            $borrowerNotification->detail = 'Pinjaman Anda telah divalidasi oleh Agen. Harap tunggu informasi selanjutnya.';
            $borrowerNotification->saveOrFail();

            $adminNotification= new AdminNotifications();
            $adminNotification->id_loan = $id_loan;
            $adminNotification->id_borrower= $confirmLoan->id_borrower;
            if($status_agency == 'Head Agent'){
                $adminNotification->id_head_agent = $id_agency;
            } else{
                $adminNotification->id_agent = $id_agency;
            }
            $adminNotification->description = 'Pinjaman Diterima Oleh Agen';
            $adminNotification->saveOrFail();

            $dt = new DateTime();
            $newDateLoan= DateLoan::where('id_loan',$id_loan)->first();
            $newDateLoan->validation_date = $dt->format('Y-m-d H:i:s');
            $newDateLoan->saveOrFail();

            $updateBorrower=DetailBorrowers::where('id_borrower', $confirmLoan->id_borrower)
                                ->first();
            $updateBorrower->borrower_loan = 'Sedang Berjalan';
            $updateBorrower->saveOrFail();
        }

 
    public function rejectLoan($id_loan, $id_agency, $loan_status_desc, $status_agency){

        $confirmLoan = Loans::where('id', $id_loan)
                    ->first();
        $confirmLoan->is_confirm_agent = 0;
        $confirmLoan->loan_status_desc = $loan_status_desc;
        // $confirmLoan->loan_status = "Pinjaman Ditolak";
        $confirmLoan->saveOrFail();
            
            // $borrowerNotification= new BorrowerNotifications();
            // $borrowerNotification->id_loan = $id_loan;
            // $borrowerNotification->id_borrower= $confirmLoan->id_borrower;
            // if($status_agency == 'Head_Agent'){
            //     $borrowerNotification->id_head_agent = $id_agency;
            // } else {
            //     $borrowerNotification->id_agent = $id_agency;
            // }
            // $borrowerNotification->description = 'Pinjaman Ditolak Oleh Agen';
            // $borrowerNotification->saveOrFail();

            $adminNotification= new AdminNotifications();
            $adminNotification->id_loan = $id_loan;
            $adminNotification->id_borrower= $confirmLoan->id_borrower;
            if($status_agency == 'Head_Agent'){
                $adminNotification->id_head_agent = $id_agency;
            } else {
                $adminNotification->id_agent = $id_agency;
            }
            $adminNotification->description = 'Pinjaman Ditolak Oleh Agen';
            $adminNotification->saveOrFail();

            // $dt = new DateTime();
            // $newDateLoan= DateLoan::where('id_loan',$id_loan)->first();
            // $newDateLoan->rejection_date = $dt->format('Y-m-d H:i:s');
            // $newDateLoan->saveOrFail();

            // $updateBorrower=DetailBorrowers::where('id_borrower', $confirmLoan->id_borrower)
            //                 ->first();
            // $updateBorrower->borrower_loan = 'Pinjaman Selesai';
            // $updateBorrower->saveOrFail();

            // $admin_fee= new AdminFee();
            // $admin_fee->id_loan = $id_loan;
            // $admin_fee->total_admin_fee = 0;
            // $admin_fee->admin_fee_percentage = "-";
            // $admin_fee->description = "Agen Menolak Pinjaman";
            // $admin_fee->is_active = 0;
            // $admin_fee->saveOrFail();
        }
    
 
public function onProgressLoan($idLoan){
    $date = new Carbon;

    $loan= DB::table('loans')
            ->where('id', $idLoan)
            ->first();    
    $now= DB::table('loans')
    ->join('installments', 'installments.id_loan','loans.id')
    ->where('loans.id', $idLoan)
    ->whereNull('installments.payment_date')
    ->first();

    $id_installment= DB::table('loans')
    ->join('installments', 'installments.id_loan','loans.id')
    ->where('loans.id', $idLoan)
    ->select('installments.id')
    ->whereNull('installments.payment_date')
    ->first();

    $paidOff= DB::table('loans')
    ->join('installments', 'installments.id_loan','loans.id')
    ->where('loans.id', $idLoan)
    ->whereNotNull('installments.payment_date')
    ->count();

    $loan->due_date= $now->due_date;
    $loan->total_paid_off= $paidOff;
    $loan->id_installment= $id_installment->id;
    $total_late_payment= (($now->installment_nominal)+($loan->installment_nominal));

    if($now->due_date < $date){
        $loan->total_late_payment= $total_late_payment;
    } else{
        $loan->total_payment= $now->installment_nominal;
    }
    return $loan;

}

    public function filterDateLoanApplicant($id_agency, $statusAgency, $starDate, $endDate){

        if($statusAgency == 'Head Agent'){
            $date = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_head_agent', $id_agency)
            ->whereNull('loans.is_confirm_agent')
            ->where('loans.loan_status','=', 'Belum Disetujui')
            ->get();
        
        } else if($statusAgency == 'Agent'){

            $date = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_agent', $id_agency)
            ->whereNull('loans.is_confirm_agent')
            ->where('loans.loan_status','=', 'Belum Disetujui')
            ->get();

        }

        return $date;

    }

    public function filterDateLoanYetApproved($id_agency, $statusAgency, $starDate, $endDate){
        if($statusAgency == 'Head Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_head_agent', $id_agency)
            ->where('loans.loan_status','=', 'Belum Disetujui')
            ->whereNull('group_name')
            ->get();

        } else if ($statusAgency == 'Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_agent', $id_agency)
            ->where('loans.loan_status','=', 'Belum Disetujui')
            ->whereNull('group_name')
            ->get();

        }
        return $dateAgent;
    }

    public function filterDateLoanApproved($id_agency, $statusAgency, $starDate, $endDate){
        if($statusAgency == 'Head Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_head_agent', $id_agency)
            ->where('loan_status','=','Pinjaman Disetujui')
            ->whereNull('group_name')
            ->get();

        } else if ($statusAgency == 'Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_agent', $id_agency)
            ->where('loan_status','=','Pinjaman Disetujui')
            ->whereNull('group_name')
            ->get();
          
        }
        return $dateAgent;
    }

    public function filterDateLoanOnProgress($id_agency, $statusAgency, $starDate, $endDate){
        if($statusAgency == 'Head Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_head_agent', $id_agency)
            ->where('loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('group_name')
            ->get();

        } else if ($statusAgency == 'Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
            ,'date_loan.submition_date'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_agent', $id_agency)
            ->where('loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('group_name')
            ->get();
        }
        return $dateAgent;
    }

    public function filterDateLoanGroup($id_agency, $statusAgency, $starDate, $endDate){
        if($statusAgency == 'Head Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','date_loan.submition_date'
            ,'date_loan.validation_date', 'date_loan.verification_date'
            ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
            ,'date_loan.cancellation_date','date_loan.rejection_date'
            ,'loans.installment_nominal','loans.tenor','loans.installment_type'
            ,'loans.loan_category','loans.group_name','loans.id_borrower'
            ,'loans.loan_status' , 'loans.loan_principal'
            ,'loans.total_borrower as total_member','loans.is_confirm_agent'
            , 'loans.is_confirm_first_admin', 'loans.is_confirm_last_admin'
            , 'loans.is_withdraw','m_borrowers.borrower_local_id','d_borrowers.name'
            ,'d_borrowers.phone_number', 'd_borrowers.rating','d_borrowers.id_card_address'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_head_agent', $id_agency)
            ->whereNotNull('group_name')
            ->get();

        } else if ($statusAgency == 'Agent'){
            $dateAgent = DB::table('date_loan')
            ->join('loans','loans.id','=','date_loan.id_loan')
            ->join('m_borrowers','m_borrowers.id','=','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=', 'm_borrowers.id')
            ->select('loans.id as id_loan','date_loan.id as id_date_loan'
            ,'m_borrowers.id as id_borrower','date_loan.submition_date'
            ,'date_loan.validation_date', 'date_loan.verification_date'
            ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
            ,'date_loan.cancellation_date','date_loan.rejection_date'
            ,'loans.installment_nominal','loans.tenor','loans.installment_type'
            ,'loans.loan_category','loans.group_name','loans.id_borrower'
            ,'loans.loan_status' , 'loans.loan_principal'
            ,'loans.total_borrower as total_member','loans.is_confirm_agent'
            , 'loans.is_confirm_first_admin', 'loans.is_confirm_last_admin'
            , 'loans.is_withdraw','m_borrowers.borrower_local_id','d_borrowers.name'
            ,'d_borrowers.phone_number', 'd_borrowers.rating','d_borrowers.id_card_address'
            )
            ->whereBetween('date_loan.submition_date',[$starDate, $endDate])
            ->where('loans.id_agent', $id_agency)
            ->whereNotNull('group_name')
            ->get();
        }
        return $dateAgent;
    }

    public function filterDateLateLoan($id_agency, $statusAgency, $starDate, $endDate){
        if($statusAgency == 'Head Agent'){
            

        } else if ($statusAgency == 'Agent'){
          
        }
        // return $dateAgent;
    }

}