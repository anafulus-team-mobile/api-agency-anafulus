<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agents;
use App\Models\DetailAgents;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;


class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function register(Request $request)
    {     
        try{
            // $mAgent = new Agents();
            // $mAgent->agent_local_id = $request->agent_local_id;
            // $mAgent->saveOrFail();

            $dAgent = new DetailAgents();
            // $dAgent->id_agent = $mAgent->id;
            $dAgent->name = $request->name; 
            $dAgent->email = $request->email; 
            $dAgent->phone_number = $request->phone_number; 
            $dAgent->registration_local_id = $request->agent_local_id;
            
            $dAgent->saveOrFail();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil mendaftar',
            ];    
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function login(Request $request)
    {     
        try{
            $statusCode = 200;
            $email=$request->email;
            $password=$request->input_password;        
            $query=DetailAgents::where('email','=',$email)->first();

            if(Hash::check($password, $query->password)){

                $response = [
                    'error' => true,
                    'message' => 'Login Berhasil',
                    'data' => [$query]
                ];    
            } else {
                $response = [
                    'error' => false,
                    'message' => 'Login Gagal11',
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Login Gagal';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // For Agent after Login
    public function changePassword(Request $request, $id_agency)
    {
        try{
            $password= $request->last_password;
            if($request->status == "Head Agent"){
                $updatePassword = DetailAgents::where('id_head_agent',$id_agency)->first();
                if(Hash::check($password, $updatePassword->password))
                {
                    $updatePassword->password = Hash::make($request->new_password); 
                    $updatePassword->saveOrFail();
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Password telah Diubah',
                    ];
                } else{
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Password gagal Diubah',
                    ];
                }
            } else if($request->status == "Agent"){

                $password= $request->last_password;
                $updatePassword = DetailAgents::where('id_agent',$id_agency)->first();

                if(Hash::check($password, $updatePassword->password))
                {
                    $updatePassword->password = Hash::make($request->new_password); 
                    $updatePassword->saveOrFail();
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Password telah Diubah',
                    ];
                }
                else{
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Password gagal Diubah',
                    ];
                }
            } else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Password Gagal Diubah',
                ];
            }
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Mengubah Password',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // for Agent after login
    public function changePhoneNumber(Request $request, $id_agency)
    {
        try{
            $lastPhone= $request->last_phone_number;
            if($request->status == 'Head Agent'){
                $phone= DetailAgents::where('id_head_agent', $id_agency)
                ->where('phone_number','=', $lastPhone)
                ->first();
                    if(!$phone){
                        $statusCode = 404;
                        $response = [
                        'error' => true,
                        'message' => 'Nomor Ponsel Salah',
                    ];
                    } else{
                        $phone->phone_number = $request->new_phone_number;
                        $phone->saveOrFail();
                        
                        $statusCode = 200;
                        $response = [
                            'error' => false,
                            'message' => 'Berhasil Mengubah Nomor Ponsel',
                        ];
                    }
            }else if($request->status == 'Agent'){
                $phone= DetailAgents::where('id_agent', $id_agency)
                        ->where('phone_number','=', $lastPhone)
                        ->first();
                        if(!$phone){
                            $statusCode = 404;
                            $response = [
                            'error' => true,
                            'message' => 'Nomor Ponsel Salah',
                        ];
                        } else{
                            $phone->phone_number = $request->new_phone_number;
                            $phone->saveOrFail();
                            
                            $statusCode = 200;
                            $response = [
                                'error' => false,
                                'message' => 'Berhasil Mengubah Nomor Ponsel',
                            ];
                        }
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Ubah Nomor Ponsel',
                ];
            }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Mengubah Nomor Ponsel',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    public function forgetPassword(Request $request)
    {
        try{
            $password= DetailAgents::where('phone_number', '=', $request->phone_number)->first();
            if(!$password){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];
            }else{
                $password->password= Hash::make($request->new_password);
                $password->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Mengubah Password',
                ];
            }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Mengubah Password',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // check Password Agent
    public function checkPassword(Request $request, $id_agency)
    {
        try{
            if($request->status == "Head Agent"){
                $query= DetailAgents::where('id_head_agent', $id_agency)->first();     
                if(Hash::check($request->password, $query->password)){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Check Password Berhasil',
                    ];
                    } else{
                        $statusCode = 404;
                        $response = [
                        'error' => true,
                        'message' => 'Gagal Check Password',
                    ];
                    }
            } else if($request->status == "Agent"){
                $query= DetailAgents::where('id_agent', $id_agency)->first();     
                if(Hash::check($request->password, $query->password)){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Check Password Berhasil',
                    ];
                }else{
                    $statusCode = 404;
                    $response = [
                    'error' => true,
                    'message' => 'Gagal Check Password',
                ];
                }
            } else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Gagal Check Password',
            ];
            }    
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Check Password',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

}