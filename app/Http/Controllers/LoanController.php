<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DateLoan;
use App\Models\Loans;
use App\Models\Borrowers;
use App\Models\DetailBorrowers;
use App\Models\DetailAgents;
use App\Models\MasterCode;
use App\Models\AdminNotifications;
use App\Models\GroupLoans;
use App\Models\BorrowerNotifications;
use Illuminate\Validation\Validator;
use App\Services\LoanService;
use App\Services\AgentService;
use App\Services\BorrowerService;
use DateTime;
use Illuminate\Support\Facades\DB;



class LoanController extends Controller
{
    public $successStatus = 200;
    protected $image_folder = '/borrower-file/';

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {   
        try{
            $list= Loans::get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Daftar Pinjaman Tersedia',
                'data' => $list,
            ];
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Daftar Pinjaman',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // Create Regular Loan By Agent
    public function createLoan(Request $request, $idAgency)
    {
        try{
            $createLoan= new Loans();
            $createLoan->id_borrower = $request->id_borrower;
            if($request->status == 'Head Agent'){
                $createLoan->id_head_agent = $idAgency;
            } else if($request->status == 'Agent'){
                $createLoan->id_agent = $idAgency;            
            }
            $createLoan->loan_category = $request->loan_category;
            $createLoan->loan_type = $request->loan_type;
            $createLoan->loan_principal = $request->loan_principal;
            $createLoan->tenor = $request->tenor;
            $createLoan->is_confirm_agent = 1;
            $createLoan->loan_status = "Belum Disetujui";
            $createLoan->installment_nominal = $request->installment_nominal;
            $createLoan->installment_type = $request->installment_type;
            $createLoan->loan_purpose = $request->loan_purpose;
            $createLoan->saveOrFail($request->all());

            $updateDetailBorrower = DetailBorrowers::where('id_borrower', $request->id_borrower)->first();                
            $updateDetailBorrower->borrower_loan = "Sedang berjalan";
            $updateDetailBorrower->phone_number = $request->phone_number;
            $updateDetailBorrower->saveOrFail();                

            $borrowerNotif= new BorrowerNotifications();
            $borrowerNotif->id_loan = $createLoan->id;
            $borrowerNotif->id_borrower = $request->id_borrower;
            if($request->status == 'Head Agent'){
                $borrowerNotif->id_head_agent = $idAgency;
            } else if($request->status == 'Agent'){
                $borrowerNotif->id_agent = $idAgency;            
            }
            $borrowerNotif->description = 'Melakukan Pinjaman Perorangan Melalui Agent';
            $borrowerNotif->detail = 'Selamat ! Pinjaman Anda telah berhasil diajukan melalui Agen. Harap tunggu informasi selanjutnya.';
            $borrowerNotif->saveOrFail($request->all());

            $adminNotif= new AdminNotifications();
            $adminNotif->id_loan = $createLoan->id;
            $adminNotif->id_borrower = $request->id_borrower;
            if($request->status == 'Head Agent'){
                $adminNotif->id_head_agent = $idAgency;
            } else if($request->status == 'Agent'){
                $adminNotif->id_agent = $idAgency;            
            }
            $adminNotif->description = 'Melakukan Pinjaman Perorangan Melalui Agent';
            $adminNotif->saveOrFail($request->all());

            $dt = new DateTime();
            $newDateLoan= new DateLoan();
            $newDateLoan->id_loan = $createLoan->id;
            $newDateLoan->submition_date = $dt->format('Y-m-d H:i:s');
            $newDateLoan->validation_date = $dt->format('Y-m-d H:i:s');
            $newDateLoan->saveOrFail($request->all());

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Menambahkan Pinjaman',
            ];     
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menambahkan Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // Create Tanggung Renteng Loan Agent
    public function createLoanGroup(Request $request, $idAgency)
    {
        try{
            $createGroupLoan= new Loans();
            $createGroupLoan->id_borrower = $request->id_leader;
            if($request->status == 'Head Agent'){
                $createGroupLoan->id_head_agent = $idAgency;
            } else if($request->status == 'Agent'){
                $createGroupLoan->id_agent = $idAgency;            
            }
            $createGroupLoan->loan_category = $request->loan_category;

            $createGroupLoan->loan_type = $request->loan_type;
            $createGroupLoan->group_name = $request->group_name;
            $createGroupLoan->total_borrower = $request->total_borrower;
            $createGroupLoan->loan_principal = $request->loan_principal;
            $createGroupLoan->tenor = $request->tenor;
            $createGroupLoan->is_confirm_agent = 1;
            $createGroupLoan->loan_status = "Belum Disetujui";
            $createGroupLoan->installment_nominal = $request->installment_nominal;
            $createGroupLoan->installment_type = $request->installment_type;
            $createGroupLoan->saveOrFail($request->all());

            $updateDetailLeaderBorrower = DetailBorrowers::where('id_borrower', $request->id_leader)->first();                
            $updateDetailLeaderBorrower->borrower_loan = "Sedang berjalan";
            $updateDetailLeaderBorrower->saveOrFail();                

            $list_id_member_group_loan = json_decode($request->list_id_member_group_loan,true);
            foreach($list_id_member_group_loan as $idBorrower){
                $group= new GroupLoans();
                $group->id_loan = $createGroupLoan->id;
                $group->id_borrower = $idBorrower;
                $group->saveOrFail($request->all());                
                
                $updateDetailMemberBorrower = DetailBorrowers::where('id_borrower', $idBorrower)->first();                
                $updateDetailMemberBorrower->borrower_loan = "Sedang berjalan";
                $updateDetailMemberBorrower->saveOrFail();                

                $borrowerNotif= new BorrowerNotifications();
                $borrowerNotif->id_loan = $createGroupLoan->id;
                $borrowerNotif->id_borrower = $idBorrower;
                if($request->status == 'Head Agent'){
                    $borrowerNotif->id_head_agent = $idAgency;
                } else if($request->status == 'Agent'){
                    $borrowerNotif->id_agent = $idAgency;            
                }
                $borrowerNotif->description = 'Melakukan Pinjaman Tanggung Renteng Melalui Agent';
                $borrowerNotif->detail = 'Selamat ! Pinjaman Tanggung Renteng Anda telah berhasil diajukan melalui Agen. Harap tunggu informasi selanjutnya.';
                $borrowerNotif->saveOrFail($request->all());

            }

            $adminNotif= new AdminNotifications();
            $adminNotif->id_loan = $createGroupLoan->id;
            $adminNotif->id_borrower = $request->id_leader;
            if($request->status == 'Head Agent'){
                $adminNotif->id_head_agent = $idAgency;
            } else if($request->status == 'Agent'){
                $adminNotif->id_agent = $idAgency;            
            }
            $adminNotif->description = 'Melakukan Pinjaman Tanggung Renteng Melalui Agent';
            $adminNotif->saveOrFail($request->all());

            $dt = new DateTime();
            $newDateLoan= new DateLoan();
            $newDateLoan->id_loan = $createGroupLoan->id;
            $newDateLoan->submition_date = $dt->format('Y-m-d H:i:s');
            $newDateLoan->validation_date = $dt->format('Y-m-d H:i:s');
            $newDateLoan->saveOrFail($request->all());

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Menambahkan Pinjaman',
            ];     
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menambahkan Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // create loan with id agent who added it
    public function createDataBorrower(Request $request, $id_agency)
    {
        try{
            $mBorrower= new Borrowers();
            $mBorrower->gender = $request->gender;
            $mBorrower->borrower_local_id = $request->borrower_local_id;
            $mBorrower->birth_date = $request->birth_date;
            $mBorrower->saveOrFail($request->all());

            $dBorrower= new DetailBorrowers();
            $dBorrower->id_borrower = $mBorrower->id;
            if($request->status == "Head Agent"){
                $dBorrower->id_head_agent = $id_agency;
            } else if($request->status == "Agent"){
                $dBorrower->id_agent = $id_agency;
            }
            $dBorrower->name= $request->name;
            $dBorrower->email= $request->email;
            $dBorrower->phone_number= $request->phone_number;
            $dBorrower->last_education= $request->last_education;
            $dBorrower->principal_taxpayer_id= $request->principal_taxpayer_id;
            $dBorrower->family_card_id= $request->family_card_id;
            $dBorrower->income = $request->income;
            $dBorrower->last_job= $request->last_job;
            $dBorrower->id_village = $request->id_village;
            $dBorrower->domicile_address = $request->domicile_address;
            $dBorrower->id_card_address = $request->id_card_address;
            // $dBorrower->id_card_postal_code = $request->id_card_postal_code;
            $dBorrower->family_phone_number = $request->family_phone_number;
            $dBorrower->family_relationship = $request->family_relationship;
            $dBorrower->family_name = $request->family_name;
            $dBorrower->borrower_loan= "Belum Pernah";

            if ($request->hasFile('id_card_selfie_image')) {
                if ($request->file('id_card_selfie_image')->isValid()) {
                    $file_ext        = $request->file('id_card_selfie_image')->getClientOriginalExtension();
                    $file_size       = $request->file('id_card_selfie_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('id_card_selfie_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $id_selfie_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('id_card_selfie_image')->move($dest_path, $id_selfie_image_name);
                        $dBorrower->id_selfie_image= $id_selfie_image_name;
                    }
                }
            }
            if ($request->hasFile('home_image')) {
                if ($request->file('home_image')->isValid()) {
                    $file_ext        = $request->file('home_image')->getClientOriginalExtension();
                    $file_size       = $request->file('home_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('home_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $home_image_name = $file_name . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('home_image')->move($dest_path, $home_image_name);
                        $dBorrower->home_image= $home_image_name;
                        // $updateDetailBorrower->home_image = $request->home_image;
                    }
                }
            }
            if ($request->hasFile('local_id_image')) {
                if ($request->file('local_id_image')->isValid()) {
                    $file_ext        = $request->file('local_id_image')->getClientOriginalExtension();
                    $file_size       = $request->file('local_id_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('local_id_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $local_id_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('local_id_image')->move($dest_path, $local_id_image_name);
                        $dBorrower->local_id_image= $local_id_image_name;
                        // $updateDetailBorrower->local_id_image = $request->local_id_image;
                    }
                }
            }
            if ($request->hasFile('salary_slip_image')) {
                if ($request->file('salary_slip_image')->isValid()) {
                    $file_ext        = $request->file('salary_slip_image')->getClientOriginalExtension();
                    $file_size       = $request->file('salary_slip_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('salary_slip_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $salary_slip_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('salary_slip_image')->move($dest_path, $salary_slip_image_name);
                        $dBorrower->salary_slip_image= $salary_slip_image_name;
                    // $updateDetailBorrower->salary_slip_image = $request->salary_slip_image;
                    }
                }
            }
            $dBorrower->saveOrFail($request->all());
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Menambahkan Data Peminjam',
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menambahkan Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateDataBorrower (Request $request, $id_borrower)
    {
        try{
            $mBorrower= Borrowers::where('id',$id_borrower)->first();
            if(!$mBorrower)
            {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];  
            } else{
                $mBorrower->borrower_local_id = $request->borrower_local_id;
                $mBorrower->gender = $request->gender;
                $mBorrower->birth_date = $request->birth_date;
                
                $mBorrower->saveOrFail($request->all());

                $dBorrower = DetailBorrowers::where('id_borrower', $id_borrower)->first();
                $dBorrower->name = $request->name;
                $dBorrower->email = $request->email;
                $dBorrower->phone_number = $request->phone_number;
                $dBorrower->family_card_id = $request->family_card_id;
                $dBorrower->principal_taxpayer_id = $request->principal_taxpayer_id;
                $dBorrower->income = $request->income;
                $dBorrower->last_education = $request->last_education;
                $dBorrower->last_job = $request->last_job;
                
                $dBorrower->saveOrFail($request->all());
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Diubah',
                ]; 
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menambahkan Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    
    public function updateAddressBorrower(Request $request, $id)
    {
        try{
            $addBorrower= DetailBorrowers::where('id_borrower', $id)->first();
            $addBorrower->id_village = $request->id_village;
            $addBorrower->domicile_address = $request->domicile_address;
            $addBorrower->id_card_address = $request->id_card_address;
            $addBorrower->saveOrFail();                         
            $statusCode = 200;
            $response = [
                'error' => true,
                'message' => 'Berhasil Update Alamat',
            ]; 
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Alamat',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateFileRequirement(Request $request, $id)
    {
        try{
            $fileBorrower = Borrowers::find($id);
            if($fileBorrower->get()->isEmpty())
            {   
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                ];  
            } else{
                $addFileBorrower = DetailBorrowers::where('id_borrower', $id)->first();                
                $addFileBorrower->family_phone_number = $request->family_phone_number;
                $addFileBorrower->family_relationship = $request->family_relationship;
                $addFileBorrower->family_name = $request->family_name;

            if ($request->hasFile('id_selfie_image')) {
                if ($request->file('id_selfie_image')->isValid()) {
                    $file_ext        = $request->file('id_selfie_image')->getClientOriginalExtension();
                    $file_size       = $request->file('id_selfie_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('id_selfie_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $id_selfie_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('id_selfie_image')->move($dest_path, $id_selfie_image_name);
                        $addFileBorrower->id_selfie_image= $id_selfie_image_name;
                    }
                }
            }
            if ($request->hasFile('home_image')) {
                if ($request->file('home_image')->isValid()) {
                    $file_ext        = $request->file('home_image')->getClientOriginalExtension();
                    $file_size       = $request->file('home_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('home_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $home_image_name = $file_name . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('home_image')->move($dest_path, $home_image_name);
                        $addFileBorrower->home_image= $home_image_name;
                        // $updateDetailBorrower->home_image = $request->home_image;
                    }
                }
            }
            if ($request->hasFile('local_id_image')) {
                if ($request->file('local_id_image')->isValid()) {
                    $file_ext        = $request->file('local_id_image')->getClientOriginalExtension();
                    $file_size       = $request->file('local_id_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('local_id_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $local_id_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('local_id_image')->move($dest_path, $local_id_image_name);
                        $addFileBorrower->local_id_image= $local_id_image_name;
                        // $updateDetailBorrower->local_id_image = $request->local_id_image;
                    }
                }
            }
            // if ($request->hasFile('family_card_image')) {
            //     if ($request->file('family_card_image')->isValid()) {
            //         $file_ext        = $request->file('family_card_image')->getClientOriginalExtension();
            //         $file_size       = $request->file('family_card_image')->getClientSize();
            //         $allow_file_exts = array('jpeg', 'jpg', 'png');
            //         $max_file_size   = 1024 * 1024 * 10;
            //         if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
            //             $dest_path     = public_path() . $this->image_folder;
            //             $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('family_card_image')->getClientOriginalName());
            //             $file_name     = str_replace(' ', '-', $file_name);
            //             $family_card_image_name = $file_name  . '.' . $file_ext;
    
            //             // move file to serve directory
            //             $request->file('family_card_image')->move($dest_path, $family_card_image_name);
            //             $addFileBorrower->local_id_image= $family_card_image_name;
            //         // $updateDetailBorrower->family_card_image = $request->family_card_image;
            //         }
            //     }
            // }
            if ($request->hasFile('salary_slip_image')) {
                if ($request->file('salary_slip_image')->isValid()) {
                    $file_ext        = $request->file('salary_slip_image')->getClientOriginalExtension();
                    $file_size       = $request->file('salary_slip_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('salary_slip_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $salary_slip_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('salary_slip_image')->move($dest_path, $salary_slip_image_name);
                        $addFileBorrower->salary_slip_image= $salary_slip_image_name;
                    // $updateDetailBorrower->salary_slip_image = $request->salary_slip_image;
                    }
                }
            }
                $fileBorrower->saveOrFail();                         
                $addFileBorrower->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Update Data Persyaratan',
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Data Persyaratan',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // view borrower profile before create loan via agent
    public function viewProfileAddLoan(Request $request)
    {
        try{
            $borrower= Borrowers::with('detailBorrower')
            ->where('id', $request->id_borrower)->first();
            $statusCode = 200;
            $response = [
                'error' => true,
                'message' => 'Profile Peminjam',
                'data' => [$borrower],
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Data Persyaratan',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function installmentCategory(Request $request, $category)
    {
        try{
            $category = MasterCode::where('category', '=', $category)->get(['name']);
            $statusCode = 200;
            $response = [
                'error' => false,
                // 'message' => 'Kategori Cicilan Tersedia',
                'data' => $category,
            ];
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Kategori Cicilan',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    public function viewByAgentCode(Request $request, $agentCode)
    {
        try {
            $agents = DetailAgents::with('agent')->where('agent_code', '=', $agentCosde)
            ->select('d_agents.id', 'd_agents.agent_code', 'd_agents.name', 'd_agents.phone_number')
            ->get();
            if($agents->isEmpty())
            {
                $statusCode = 200;
                $response = [
                    'message' => 'Agent Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Agent Tersedia',
                    'data' => $agents,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Agent',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function viewBySubdistrict(Request $request, $idSubdistrict)
    {
        try {
            $agents = DetailAgents::with('agent')->where('id_sub_district', $idSubdistrict)
            ->select('d_agents.id', 'd_agents.agent_code', 'd_agents.name', 'd_agents.phone_number')
            ->get();
            if($agents->isEmpty())
            {
                $statusCode = 200;
                $response = [
                    'message' => 'Agent Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Agent Tersedia',
                    'data' => $agents,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Agent',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // VIew all Loan based on their status
    public function filterAllByStatus (Request $request, $id)
    {
        try{   
            $loanStatus = Loans::where('id_agent', $id)
            ->where('loan_status','=',$request->loan_status)->get();
            if(!$loanStatus){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                ];
            } else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Pinjaman',
                    'data' => $loanStatus,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Status Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateBalance(Request $request, $id)
    {
        try{
            $updateLoan = Loans::where('id_borrower', $id)->first();
            $updateLoan->loan_principal = $request->loan_principal;
            $updateLoan->saveOrFail();
            $updateBorrower = DetailBorrowers::where('id_borrower', $id)->first();
            $updateBorrower->finance_balance = $request->finance_balance;
            $updateBorrower->saveOrFail();
            $statusCode = 200;
            $response = [
            'error' => true,
            'message' => 'Update Saldo',
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
            'error' => true,
            'message' => 'Gagal Update Saldo',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // ///////////////////////////////////////////////////////////////

    public function confirmLoan(Request $request, $id_loan)
    {   
        try {
            $loan= new LoanService();
            $confirmLoan = $loan->confirmLoan($id_loan, $request->id_agency, $request->status);
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Konfirmasi Pinjaman Berhasil',
            ];

        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => ' Gagal Konfirmasi Pinjaman',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function rejectLoan(Request $request, $id_loan)
    {
        try{
            $loan= new LoanService();

            $rejectLoan = $loan->rejectLoan($id_loan, $request->id_agency, $request->rejected_reason, $request->status);
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Pinjaman Telah Ditolak',
            ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => ' Gagal Tolak Pinjaman',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    //  View list borrower yang di satu kecamatan dengan agent
    // sedang dalam status Pinjaman Sudah Selesai, Pinjaman Ditolak, Pinjaman Dibatalkan
    public function viewBorrowerGroup(Request $request, $id)
    {
       try{
           $borrowerService= new BorrowerService();
           $borrower = $borrowerService->viewBorrowerGroup($id);
           if(!$borrower){
               $statusCode = 404;
               $response = [
               'error' => true,
               'message' => 'Data Tidak Ada',
               ];   
           }else{
               $statusCode = 200;
               $response = [
               'error' => false,
               'message' => 'Daftar Peminjam Layak Tanggung Renteng',
               'data' =>$borrower,
               ];
           }   
       }catch (Exception $ex) {
           $statusCode = 404;
           $response = [
               'error' => true,
               'message' => 'Gagal Tampilkan Data Peminjam',
           ];
       }
       finally {
           return response($response,$statusCode)->header('Content-Type','application/json');
       }
    }

    public function updateLateInstallment(Request $request, $id)
    {
        try{
            // $installment= ;
        }catch (Exception $ex) {
           $statusCode = 404;
           $response = [
               'error' => true,
               'message' => 'Gagal Tampilkan Data Peminjam',
           ];
       }
       finally {
           return response($response,$statusCode)->header('Content-Type','application/json');
       }
    }

    public function searchBorrowerLocalId(Request $request, $id)
    {
        try{
            $loanService= new LoanService();
            $search = $loanService->searchLocalId($id, $request->borrower_local_id);
            if(!$search){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Hasil Pencarian',
                    'data' => $search,
                ];
            }
        }catch (Exception $ex) {
           $statusCode = 404;
           $response = [
               'error' => true,
               'message' => 'Gagal Tampilkan Data Peminjam',
           ];
       }
       finally {
           return response($response,$statusCode)->header('Content-Type','application/json');
       }
    }


        // create loan with id agent who added it
        public function createDataBorrowerAndLoan(Request $request, $id_agency)
        {
            try{
                $mBorrower= new Borrowers();
                $mBorrower->gender = $request->gender;
                $mBorrower->borrower_local_id = $request->borrower_local_id;
                $mBorrower->birth_date = $request->birth_date;
                $mBorrower->saveOrFail($request->all());
    
                $dBorrower= new DetailBorrowers();
                $dBorrower->id_borrower = $mBorrower->id;
                if($request->status == "Head Agent"){
                    $dBorrower->id_head_agent = $id_agency;
                } else if($request->status == "Agent"){
                    $dBorrower->id_agent = $id_agency;
                }
                $dBorrower->name= $request->name;
                $dBorrower->email= $request->email;
                $dBorrower->phone_number= $request->phone_number;
                $dBorrower->last_education= $request->last_education;
                $dBorrower->principal_taxpayer_id= $request->principal_taxpayer_id;
                $dBorrower->family_card_id= $request->family_card_id;
                $dBorrower->income = $request->income;
                $dBorrower->last_job= $request->last_job;
                $dBorrower->id_village = $request->id_village;
                $dBorrower->domicile_address = $request->domicile_address;
                $dBorrower->id_card_address = $request->id_card_address;
                // $dBorrower->id_card_postal_code = $request->id_card_postal_code;
                $dBorrower->family_phone_number = $request->family_phone_number;
                $dBorrower->family_relationship = $request->family_relationship;
                $dBorrower->family_name = $request->family_name;
                $dBorrower->borrower_loan= "Belum Pernah";
    
                if ($request->hasFile('id_card_selfie_image')) {
                    if ($request->file('id_card_selfie_image')->isValid()) {
                        $file_ext        = $request->file('id_card_selfie_image')->getClientOriginalExtension();
                        $file_size       = $request->file('id_card_selfie_image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = public_path() . $this->image_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('id_card_selfie_image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $id_selfie_image_name = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('id_card_selfie_image')->move($dest_path, $id_selfie_image_name);
                            $dBorrower->id_selfie_image= $id_selfie_image_name;
                        }
                    }
                }
                if ($request->hasFile('home_image')) {
                    if ($request->file('home_image')->isValid()) {
                        $file_ext        = $request->file('home_image')->getClientOriginalExtension();
                        $file_size       = $request->file('home_image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = public_path() . $this->image_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('home_image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $home_image_name = $file_name . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('home_image')->move($dest_path, $home_image_name);
                            $dBorrower->home_image= $home_image_name;
                            // $updateDetailBorrower->home_image = $request->home_image;
                        }
                    }
                }
                if ($request->hasFile('local_id_image')) {
                    if ($request->file('local_id_image')->isValid()) {
                        $file_ext        = $request->file('local_id_image')->getClientOriginalExtension();
                        $file_size       = $request->file('local_id_image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = public_path() . $this->image_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('local_id_image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $local_id_image_name = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('local_id_image')->move($dest_path, $local_id_image_name);
                            $dBorrower->local_id_image= $local_id_image_name;
                            // $updateDetailBorrower->local_id_image = $request->local_id_image;
                        }
                    }
                }
                if ($request->hasFile('salary_slip_image')) {
                    if ($request->file('salary_slip_image')->isValid()) {
                        $file_ext        = $request->file('salary_slip_image')->getClientOriginalExtension();
                        $file_size       = $request->file('salary_slip_image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = public_path() . $this->image_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('salary_slip_image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $salary_slip_image_name = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('salary_slip_image')->move($dest_path, $salary_slip_image_name);
                            $dBorrower->salary_slip_image= $salary_slip_image_name;
                        // $updateDetailBorrower->salary_slip_image = $request->salary_slip_image;
                        }
                    }
                }
                $dBorrower->saveOrFail($request->all());


                ///create loan after create borrower
                $createLoan= new Loans();
                $createLoan->id_borrower = $dBorrower->id_borrower;
                if($request->status == 'Head Agent'){
                    $createLoan->id_head_agent = $id_agency;
                } else if($request->status == 'Agent'){
                    $createLoan->id_agent = $id_agency;            
                }    
                $createLoan->loan_category = $request->loan_category;
                $createLoan->loan_type = "Perorangan - Agent App";
                $createLoan->loan_principal = $request->loan_principal; //masalah
                $createLoan->installment_nominal = $request->installment_nominal; // masalah
                $createLoan->tenor = $request->tenor;
                $createLoan->is_confirm_agent = 1;
                $createLoan->loan_status = "Belum Disetujui";
                $createLoan->installment_type = $request->installment_type;
                $createLoan->loan_purpose = $request->loan_purpose;
                $createLoan->saveOrFail($request->all());
    
                $updateDetailBorrower = DetailBorrowers::where('id_borrower', $dBorrower->id_borrower)->first();                
                $updateDetailBorrower->borrower_loan = "Sedang berjalan";
                $updateDetailBorrower->saveOrFail();                
    
                $adminNotif= new AdminNotifications();
                $adminNotif->id_loan = $createLoan->id;
                $adminNotif->id_borrower = $dBorrower->id_borrower;
                if($request->status == 'Head Agent'){
                    $adminNotif->id_head_agent = $id_agency;
                } else if($request->status == 'Agent'){
                    $adminNotif->id_agent = $id_agency;            
                }
                $adminNotif->description = 'Melakukan Pinjaman Perorangan Melalui Agent';
                $adminNotif->saveOrFail($request->all());
    
                $dt = new DateTime();
                $newDateLoan= new DateLoan();
                $newDateLoan->id_loan = $createLoan->id;
                $newDateLoan->submition_date = $dt->format('Y-m-d H:i:s');
                $newDateLoan->validation_date = $dt->format('Y-m-d H:i:s');
                $newDateLoan->saveOrFail($request->all());
    
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Menambahkan Data Peminjam',
                ];
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Menambahkan Pinjaman',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }

        public function filterDateBorrowerApplicant(Request $request, $id_agency){
            try{
                $starDate= $request->start_date;
                $endDate= $request->end_date;
                $statusAgency= $request->status_agency;

                $service= new LoanService();
                $filter= $service->filterDateLoanApplicant($id_agency, $statusAgency, $starDate, $endDate);
                if($filter->isEmpty()){
                    $statusCode = 404;
                    $response = [
                    'error' => true,
                    'message' => 'Data tidak ada',
                    ];
                }else{
                    $statusCode = 200;
                    $response = [
                    'error' => false,
                    'message' => 'Berhasil',
                    'dataFilterApplicant' => $filter,
                    ];
                }

            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }
    

        public function filterDateLoanYetApproved(Request $request, $id_agency){
            try{
                $starDate= $request->start_date;
                $endDate= $request->end_date;
                $statusAgency= $request->status_agency;
                $service= new LoanService();
                $filter= $service->filterDateLoanYetApproved($id_agency, $statusAgency, $starDate, $endDate);
                    if($filter->isEmpty()){
                        $statusCode = 404;
                        $response = [
                        'error' => true,
                        'message' => 'Data tidak ada',
                        ];
                    }else{
                        $statusCode = 200;
                        $response = [
                        'error' => false,
                        'message' => 'Berhasil',
                        'dataFilterLoanYetApproved' => $filter,
                        ];
                    }
            
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }

        public function filterDateLoanApproved(Request $request, $id_agency){
            try{
                $starDate= $request->start_date;
                $endDate= $request->end_date;
                $statusAgency= $request->status_agency;
                $service= new LoanService();
                $filter= $service->filterDateLoanApproved($id_agency, $statusAgency, $starDate, $endDate);
                    if($filter->isEmpty()){
                        $statusCode = 404;
                        $response = [
                        'error' => true,
                        'message' => 'Data tidak ada',
                        ];
                    }else{
                        $statusCode = 200;
                        $response = [
                        'error' => false,
                        'message' => 'Berhasil',
                        'dataFilterLoanApproved' => $filter,
                        ];
                    }
            
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }

        public function filterDateLoanOnProgress(Request $request, $id_agency){
            try{
                $starDate= $request->start_date;
                $endDate= $request->end_date;
                $statusAgency= $request->status_agency;
                $service= new LoanService();
                $filter= $service->filterDateLoanOnProgress($id_agency, $statusAgency, $starDate, $endDate);
                    if($filter->isEmpty()){
                        $statusCode = 404;
                        $response = [
                        'error' => true,
                        'message' => 'Data tidak ada',
                        ];
                    }else{
                        $statusCode = 200;
                        $response = [
                        'error' => false,
                        'message' => 'Berhasil',
                        'dataFilterLoanOnProgress' => $filter,
                        ];
                    }
            
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }

        public function filterDateLoanGroup(Request $request, $id_agency){
            try{
                $starDate= $request->start_date;
                $endDate= $request->end_date;
                $statusAgency= $request->status_agency;
                $service= new LoanService();
                $filter= $service->filterDateLoanGroup($id_agency, $statusAgency, $starDate, $endDate);
                    if($filter->isEmpty()){
                        $statusCode = 404;
                        $response = [
                        'error' => true,
                        'message' => 'Data tidak ada',
                        ];
                    }else{
                        $statusCode = 200;
                        $response = [
                        'error' => false,
                        'message' => 'Berhasil',
                        'dataFilterLoanGroup' => $filter,
                        ];
                    }
            
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }

        public function filterDateLateLoan(Request $request, $id_agency){
            try{
                $starDate= $request->start_date;
                $endDate= $request->end_date;
                $statusAgency= $request->status_agency;
                $service= new LoanService();
                $filter= $service->filterDateLateLoan($id_agency, $statusAgency, $starDate, $endDate);
                    if($filter->isEmpty()){
                        $statusCode = 404;
                        $response = [
                        'error' => true,
                        'message' => 'Data tidak ada',
                        ];
                    }else{
                        $statusCode = 200;
                        $response = [
                        'error' => false,
                        'message' => 'Berhasil',
                        'dataFilterLateLoan' => $filter,
                        ];
                    }
            
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }

}

