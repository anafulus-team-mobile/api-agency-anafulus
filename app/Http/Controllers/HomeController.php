<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promotions;
use App\Models\Agents;
use App\Models\HeadAgents;
use App\Models\Loans;
use App\Models\GroupLoans;
use App\Models\DetailBorrowers;
use App\Models\DetailAgents;
use App\Models\KospinLocation;
use App\Models\BankAccounts;
use App\Models\FormulaCommissions;
use App\Models\CompanyInformations;
use App\Models\AgentNotifications;
use App\Models\Questions;
use App\Models\SubDistricts;
use App\Models\Helps;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;
use App\Services\LoanService;
use App\Services\AgentService;

class HomeController extends Controller
{
    public $successStatus = 200;

    public function __construct(){
        $this->middleware('auth');
    }

    public function viewPromotion(Request $request)
    {   
        try{
            $images= Promotions::where('is_active', 1)->get();
            if(!$images){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Foto Promosi Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Foto Promosi Tersedia',
                'dataPromotions' => $images,
            ];
            }
            
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Foto Promosi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewKospinLocation(Request $request)
    {
        try{
            $kospin= KospinLocation::where('id_province', $request->province)->get();
            $statusCode = 404;
                $response = [
                        'error' => false,
                        'message' => 'Daftar Lokasi Kospin',
                        'data' => $kospin,
                ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Lokasi Kospin',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View all agents information in one district with Head Agent
    public function viewAllAgents(Request $request, $idSubdistrict)
    {
        try {
            $listAgent = DetailAgents::join('m_agents', 'm_agents.id','=','d_agents.id_agent')
            ->where('id_sub_district', $idSubdistrict)
            ->whereNotNull('id_agent')->get();
            foreach ($listAgent as $data) {
                $totalBorrower= Loans::where('id_agent', $data->id_agent)
                        ->where('group_name', '=',null)
                        // ->where('loan_status', '=', 'Cicilan sedang berjalan')
                        // ->where(function ($query) {
                        //     $query->where('loan_status', '=', 'Cicilan sedang berjalan');
                        //         //   ->orwhere('loan_status', '=', 'Belum Disetujui')
                        //         //   ->orwhere('loan_status', '=', 'Pinjaman Disetujui');
                        // })
                        ->count();
                $data->total_regular_loan = $totalBorrower;
                $finalData[] = $data;
            }

            if(!$finalData)
            {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Agent Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Agent Dalam Satu Kecamatan',
                    'listAgent' => $finalData,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Agent',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    public function viewHeadAgent(Request $request, $id)
    {
        try{
        $headAgent= DetailAgents::where('id_sub_district', $id)
                    ->whereNotNull('id_head_agent')
                    ->first();
        $sumAgent = DetailAgents::where('id_sub_district', $id)
                    ->whereNotNull('id_agent')->count();
        $areaAgent = SubDistricts::where('id', $id)->first();

        $headAgent->total_agent = $sumAgent;

        $headAgent->area_agent = $areaAgent->sub_district_name;

        if(!$headAgent)
            {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Head Agent Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Head Agent Satu Kecamatan',
                    'dataHeadAgent' => [$headAgent]
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Head Agent',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }   

    
    public function viewMemberTanggungRenteng(Request $request, $idLoan)
    {
        try{
            $memberTanggungRenteng= GroupLoans::join('m_borrowers','m_borrowers.id','=','group_loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower','=','m_borrowers.id')
            ->select('m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number', 'd_borrowers.rating'
                    ,'d_borrowers.id_card_address')
            ->where('id_loan', $idLoan)
            ->get();                
            $statusCode = 200;
            $response = [
                'error' => true,
                'message' => 'Anggota tanggung renteng Ditampilkan',
                'dataMemberTanggungRenteng' => $memberTanggungRenteng,
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan anggota tanggung renteng',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View Loans of Tanggung Renteng handled by Agent
    public function viewLoanTanggungRenteng(Request $request, $idAgency)
    {
        try{
            if($request->status == "Head Agent"){
                $loanService= new LoanService();
                $loans = $loanService->viewGroupLoanHead($idAgency);
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Pinjaman Tanggung Renteng',
                'dataTanggungRentengHeadAgent' => $loans,
                ];
            }else if($request->status == "Agent"){
                $loanService= new LoanService();
                $loans = $loanService->viewGroupLoan($idAgency);
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Pinjaman Tanggung Renteng',
                'dataTanggungRentengHeadAgent' => $loans,
            ];
        }else{
        $statusCode = 404;
        $response = [
            'error' => true,
            'message' => 'Data Tidak Ada',
        ];
        }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Kelompok Tanggung Renteng',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    //    Search Group Name of Tanggung Renteng and See Detail
    public function detailTanggungRenteng(Request $request, $id)
    {
        try{
            if($request->status == "Head Agent"){
            $loanService= new LoanService();
            $loans = $loanService->detailGroupLoanHead($id, $request->id_group_loan);
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Detail Tanggung Renteng',
                'data' => $loans,
            ];
            } else if($request->status == "Agent"){
            $loanService= new LoanService();
            $loans = $loanService->detailGroupLoan($id, $request->id_group_loan);
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Detail Tanggung Renteng',
                'data' => $loans,
            ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];
            }            
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Kelompok Tanggung Renteng',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View borrowers that have not confirmed by agent (Calon Peminjam)
    public function viewBorrowerApplicant(Request $request, $id)
    {   
        try{
            if ($request->status == "Head Agent") {
                $agentService= new AgentService();
                $borrowerApplicant= $agentService->viewBorrowerApplicantHead($id); 
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Daftar Calon Peminjam Belum Dikonfirmasi',
                'dataBorrowers' => $borrowerApplicant,
            ];
            } else if($request->status == "Agent"){
                $agentService= new AgentService();
                $borrowerApplicant= $agentService->viewBorrowerApplicant($id);
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Daftar Calon Peminjam Belum Dikonfirmasi',
                'dataBorrowers' => $borrowerApplicant,
            ];
            } else {
                $statusCode = 404;
                $response = [
                        'error' => true,
                        'message' => 'Daftar Calon Peminjam Tidak Tersedia',
                ];
            }
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Daftar Calon Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    public function viewRegularLoan(Request $request, $id)
    {
        try{            
            $regulerLoan= new AgentService();
            $lateLoan= new LoanService();
            if($request->status == "Head Agent"){
                // dd("test");
                if($request->loan_status == "Belum Disetujui"){
                    $detailRegulerLoan= $regulerLoan->notYetLoanHead($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Belum Disetujui',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }
                else if($request->loan_status == "Pinjaman Disetujui"){
                    $detailRegulerLoan= $regulerLoan->approvedLoanHead($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Disetujui',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }
                else if($request->loan_status == "Cicilan Sedang Berjalan"){
                    $detailRegulerLoan= $regulerLoan->onProgressLoanHead($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Cicilan Sedang Berjalan',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }
                else if($request->loan_status == "Cicilan Terlambat"){
                    $detailRegulerLoan= $lateLoan->viewLateLoanHead($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Cicilan Sedang Berjalan',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }

            }
            else if($request->status == "Agent"){
                if($request->loan_status == "Belum Disetujui"){
                    $detailRegulerLoan= $regulerLoan->notYetLoan($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Belum Disetujui',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }
                else if($request->loan_status == "Pinjaman Disetujui"){
                    $detailRegulerLoan= $regulerLoan->approvedLoan($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Disetujui',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }
                else if($request->loan_status == "Cicilan Sedang Berjalan"){
                    $detailRegulerLoan= $regulerLoan->onProgressLoan($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Cicilan Sedang Berjalan',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }
                else if($request->loan_status == "Cicilan Terlambat"){
                    $detailRegulerLoan= $lateLoan->viewLateLoan($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Cicilan Sedang Berjalan',
                        'dataRegulerLoan' => $detailRegulerLoan,
                    ];
                }

            }          
             else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];
            }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Daftar  Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    public function filterByCategory(Request $request)
    {
        try{
            $listCategory = Loans::with('borrower')->with('detailBorrower')
            ->where('loan_category', '=', $request->loan_category)->get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Tampilan Pinjaman Berdasarkan Kategori',
                'data' => [$listCategory],
            ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Tampilan Kategori Pinjaman Gagal',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function filterByType(Request $request)
    {
        try{
            $listType = Loans::with('borrower')->with('detailBorrower')
            ->where('loan_type', '=', $request->loan_type)->get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Tampilan Berdasarkan Jenis Pinjaman',
                'data' => [$listType],
            ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Tampilan Jenis Pinjaman Gagal',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function filterByRating(Request $request)
    {
        try{
            $listRating = DetailBorrowers::with('borrower')
            ->where('rating', '=', $request->rating)->get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Tampilan  Rating Borrower',
                'data' => [$listRating],
            ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Rating Borrower',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
        
    public function viewInformation(Request $request, $name)
    {   
        try{
            $inform = CompanyInformations::where('name','=',$name)->get();
            $statusCode = 200;
                $response = [
                'error' => false,
                'data' => [$inform],
            ];
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Informasi Perusahaan',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // view all Head Agent Notification 
    public function viewNotificationAgency(Request $request, $id_agency)
    {   
        try{
            $service = new AgentService();
            if($request->status == "Head Agent"){
                $notif= $service->viewNotificationHead($id_agency);
                $statusCode = 200;
                $response = [
                'error' => false,
                'dataNotifications' => $notif,
            ];
            } else if($request->status == "Agent"){
                $notif= $service->viewNotification($id_agency);
                $statusCode = 200;
                $response = [
                'error' => false,
                'dataNotifications' => $notif,
            ];
            }
            else{
                $statusCode = 404;
                $response = [
                'error' => true,
            ];  
            }
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function detailNotifTransaction($idTransaction){
        try{
            $service = new AgentService();
            $notif= $service->notifTransaction($idTransaction);

            if(!$notif){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];  
            }
            else{
                $statusCode = 200;
                $response = [
                'error' => false,
                'detailTransaction' => [$notif],
            ];
            }

        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function deleteNotification($id_notif){
        try{
            $notif= AgentNotifications::where('id', $id_notif)->delete();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Menghapus Notifikasi',
            ];
            
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menghapus Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function readNotification($id_notif){
        try{
            $notif= AgentNotifications::where('id', $id_notif)->first();
            $notif->is_read= '1';
            $notif->saveOrFail();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Baca Notifikasi',
            ];
            
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Baca Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewHelp(Request $request)
    {
        try{
            $question= Questions::where('category',$request->category)->get(); 
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Help Page',
                    'data' => $question,
                ];
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Help Page',
                ];
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function viewHelpDetail(Request $request, $id)
    {
        try{
            $question= Questions::where('id',$id)->first(); 
            $answer= Helps::where('id_question',$id)->get();
            if(!$answer)
            {   $statusCode = 200;
                $response = [
                    'message' => 'Tidak Terdapat Bantuan',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Help Page',
                    'Question' => $question->question,
                    'data' => $answer,
                ];
                }
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Help Page',
                ];
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function historyAddLoan(Request $request, $idAgency){
        try{
            $history= new AgentService();
            $loanService= new LoanService();
            if($request->status == "Head Agent"){
                if($request->loan_type == 'Perorangan - Agent App')
                {
                    $list= $history->historyIndividuHead($idAgency, $request->loan_type);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'History',
                        'dataHistory' => $list,
                    ];
                } else if($request->loan_type == 'Tanggung Renteng')
                {
                    $group = $loanService->viewGroupLoanHead($idAgency);
                    // $list= $history->historyGroupHead($idAgency, $request->loan_type);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'History',
                        'dataHistory' => $group,
                    ];
                }

            } else if($request->status == "Agent"){
                if($request->loan_type == "Perorangan - Agent App")
                {

                    $list= $history->historyIndividu($idAgency, $request->loan_type);

                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Help Page',
                        'dataHistory' => $list,
                    ];
                } else if($request->loan_type == 'Tanggung Renteng')
                {
                    $group = $loanService->viewGroupLoan($idAgency);
                    // $list= $history->historyGroup($idAgency, $request->loan_type);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Help Page',
                        'dataHistory' => $group,
                    ];
                }
            } else {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Riwayat Tidak Ada',
                ];
                }

        }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Help Page',
                ];
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

     // Search Applicant Borrower based on local id number
     public function searchBorrowerApplicant(Request $request, $id_agency)
     {
         try{
             $agentService= new AgentService();
             if ($request->status == "Head Agent") {
                 $borrowerApplicant= $agentService->searchBorrowerApplicantHead($id_agency, $request->borrower_local_id); 
                 $statusCode = 200;
                 $response = [
                 'error' => false,
                 'message' => 'Berhasil Mencari Calon Peminjam',
                 'dataSearchApplicantBorrower' => [$borrowerApplicant],
             ];
             } else if($request->status == "Agent"){
                 $borrowerApplicant= $agentService->searchBorrowerApplicant($id_agency, $request->borrower_local_id);
                 $statusCode = 200;
                 $response = [
                 'error' => false,
                 'message' => 'Berhasil Mencari Calon Peminjam',
                 'dataSearchApplicantBorrower' => [$borrowerApplicant],
             ];
             } else {
                 $statusCode = 404;
                 $response = [
                         'error' => true,
                         'message' => 'Daftar Calon Peminjam Tidak Tersedia',
                 ];
             }
         }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
     }

     public function searchRegularLoan(Request $request, $id_agency)
    {
        try{            
            $regulerLoan= new AgentService();
            $lateLoan= new LoanService();
            if($request->status == "Head Agent"){
                if($request->loan_status == "Belum Disetujui"){
                    $detailRegulerLoan= $regulerLoan->searchNotYetLoanHead($id_agency, $request->borrower_local_id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Belum Disetujui',
                        'dataSearchRegulerLoan' => [$detailRegulerLoan],
                    ];
                }
                else if($request->loan_status == "Pinjaman Disetujui"){
                    $detailRegulerLoan= $regulerLoan->searchApprovedLoanHead($id_agency, $request->borrower_local_id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Disetujui',
                        'dataSearchRegulerLoan' => [$detailRegulerLoan],
                    ];
                }
                else if($request->loan_status == "Cicilan Sedang Berjalan"){
                    $detailRegulerLoan= $regulerLoan->searchOnProgressLoanHead($id_agency, $request->borrower_local_id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Cicilan Sedang Berjalan',
                        'dataSearchRegulerLoan' => [$detailRegulerLoan],
                    ];
                }
                // else if($request->loan_status == "Cicilan Terlambat"){
                    // $detailRegulerLoan= $lateLoan->viewLateLoan($id);
                    // $statusCode = 200;
                    // $response = [
                    //     'error' => false,
                    //     'message' => 'Detail Cicilan Sedang Berjalan',
                    //     'dataSearchRegulerLoan' => $detailRegulerLoan,
                    // ];
                // }

            }
            else if($request->status == "Agent"){
                if($request->loan_status == "Belum Disetujui"){
                    $detailRegulerLoan= $regulerLoan->searchNotYetLoan($id_agency, $request->borrower_local_id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Belum Disetujui',
                        'dataSearchRegulerLoan' => [$detailRegulerLoan],
                    ];
                }
                else if($request->loan_status == "Pinjaman Disetujui"){
                    $detailRegulerLoan= $regulerLoan->searchApprovedLoan($id_agency, $request->borrower_local_id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Pinjaman Disetujui',
                        'dataSearchRegulerLoan' => [$detailRegulerLoan],
                    ];
                }
                else if($request->loan_status == "Cicilan Sedang Berjalan"){
                    $detailRegulerLoan= $regulerLoan->searchOnProgressLoan($id_agency, $request->borrower_local_id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Detail Cicilan Sedang Berjalan',
                        'dataSearchRegulerLoan' => [$detailRegulerLoan],
                    ];
                }
                // else if($request->loan_status == "Cicilan Terlambat"){
                    // $detailRegulerLoan= $lateLoan->viewLateLoan($id);
                    // $statusCode = 200;
                    // $response = [
                    //     'error' => false,
                    //     'message' => 'Detail Cicilan Sedang Berjalan',
                    //     'dataSearchRegulerLoan' => $detailRegulerLoan,
                    // ];
                // }

            }          
             else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];
            }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Daftar  Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllAgencies()
    {
        try {
            $list= DetailAgents::get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Agen Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Agen',
                    'dataAllAgencies' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getMasterHeadAgent()
    {
        try {
            $list= HeadAgents::get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Agen Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Agen',
                    'dataMasterHeadAgent' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getMasterAgent()
    {
        try {
            $list= Agents::get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Agen Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Agen',
                    'dataMasterAgent' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllBorrowers()
    {
        try {
            $list= DetailBorrowers::get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Peminjam Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Peminjam',
                    'data' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    
}