<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agents;
use App\Models\HeadAgents;
use App\Models\Loans;
use App\Models\DetailBorrowers;
use App\Models\DetailAgents;
use App\Models\AgentNotifications;
use App\Models\KospinLocation;
use App\Models\BankAccounts;
use App\Models\Questions;
use App\Models\Villages;
use App\Models\Installments;
use App\Models\Provinces;
use App\Models\Regencies;
use App\Models\SubDistricts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;
use App\Services\BorrowerService;
use App\Services\AgentService;

class AgencyController extends Controller
{
    public $successStatus = 200;
    protected $image_profile_folder = '/image/';

    public function __construct(){
        $this->middleware('auth');
    }

///////////////Agency Profile//////////////////  
public function updatePersonalData(Request $request, $id)
{
    try{
        if ($request->status == "Head Agent") {
            $updateMHeadAgent = HeadAgents::find($id);        
            $updateMHeadAgent->gender = $request->gender;
            $updateMHeadAgent->birth_date = $request->birth_date; 
            $updateMHeadAgent->agent_local_id = $request->agent_local_id; 
            $updateMHeadAgent->saveOrFail();

            $updateDHeadAgent = DetailAgents::where('id_head_agent', $id)->first();   
            $updateDHeadAgent->name = $request->name;
            $updateDHeadAgent->email = $request->email;
            $updateDHeadAgent->phone_number = $request->phone_number;
            $updateDHeadAgent->agent_family_card_id = $request->agent_family_card_id;
            $updateDHeadAgent->principal_taxpayer_id = $request->principal_taxpayer_id;
            $updateDHeadAgent->id_card_address = $request->id_card_address;
            $updateDHeadAgent->id_sub_district = $request->id_sub_district;
            $updateDHeadAgent->last_education = $request->last_education;
            $updateDHeadAgent->saveOrFail();
        } else if ($request->status == "Agent"){ 
            $updateMAgent = Agents::find($id);        
            $updateMAgent->gender = $request->gender;
            $updateMAgent->birth_date = $request->birth_date;
            $updateMAgent->agent_local_id = $request->agent_local_id; 
            $updateMAgent->saveOrFail();

            $updateDAgent = DetailAgents::where('id_agent', $id)->first();                
            $updateDAgent->name = $request->name;
            $updateDAgent->email = $request->email;
            $updateDAgent->phone_number = $request->phone_number;
            $updateDAgent->agent_family_card_id = $request->agent_family_card_id;
            $updateDAgent->principal_taxpayer_id = $request->principal_taxpayer_id;
            $updateDAgent->id_card_address = $request->id_card_address;
            $updateDAgent->id_sub_district = $request->id_sub_district;
            $updateDAgent->last_education = $request->last_education;
            $updateDAgent->saveOrFail();        
        }
        $statusCode = 200;
        $response = [
            'error' => false,
            'message' => 'Berhasil Update Data Pribadi'
        ];    
    } catch (Exception $ex) {
        $statusCode = 404;
        $response = [
            'error' => true,
            'message' => 'Gagal Update Data Probadi',
        ];  
    }
    finally {
        return response($response,$statusCode)->header('Content-Type','application/json');
    }
}

public function viewProfileAgency (Request $request, $id)
{
    try{
        $agentService = new AgentService();
        if ($request->status == "Head Agent") {
            $headAgent= $agentService->getProfileHeadAgent($id);
            $statusCode = 200;
            $response = [
            'error' => false,
            'message' => 'Data Pribadi Agent',
            'data' => [$headAgent],
        ];
        } else if ($request->status == "Agent"){
            $agent= $agentService->getProfileAgent($id);
            $statusCode = 200;
            $response = [
            'error' => false,
            'message' => 'Data Pribadi Agent',
            'data' => [$agent],
        ];
        }    
        else{
            $statusCode = 404;
            $response = [
            'error' => true,
            'message' => 'Data Tidak Ditemukan',
        ];
        }
    }catch (Exception $ex) {
        $statusCode = 404;
        $response = [
            'error' => false,
            'message' => 'Gagal View Data Pribadi',
        ];  
    }
    finally {
        return response($response,$statusCode)->header('Content-Type','application/json');
    }
}

// History agen berisi komisi (saldo bertambah, saldo berkurang, saldo ditarik, peminjaman
// yg melalui agen)
public function viewHistoryHead(Request $request, $id)
{
    try{
        $listHistory = AgentNotifications::
        where('id_head_agent',$id)
        ->where('topic', '=', $request->topic)
        ->get();
        $statusCode = 200;
        $response = [
            'error' => false,
            'message' => 'Tampilkan Riwayat Pinjaman',
            'data' => $listHistory,
        ];
    }catch (Exception $ex){
        $statusCode = 404;
        $response = [
            'error' => true,
            'message' => 'Tampilan Riwayat Pinjaman Gagal',
        ];
    }
    finally {
        return response($response,$statusCode)->header('Content-Type','application/json');
    }
}
public function viewLoanHead(Request $request, $id)
{   
    try{
        $loans= Loans::with('detailBorrower')->where('id_head_agent', $id)->get();        
        if($loans->isEmpty()){
            $statusCode = 404;
            $response = [
                    'error' => true,
                    'message' => 'Daftar Pinjaman Tidak Tersedia',
            ];
        } else {
            $statusCode = 200;
        $response = [
            'error' => false,
            'message' => 'Daftar Pinjaman Tersedia',
            'data' => $loans,
        ];
        }
    } catch (Exception $ex){
        $statusCode = 404;
        $response = [
            'error' => true,
            'message' => 'Gagal Menampilkan Daftar Pinjaman',
        ];
    }
    finally {
        return response($response,$statusCode)->header('Content-Type','application/json');
    }
}

public function viewBankAccountAgency(Request $request, $id)
{
    try {
            $agentService= new AgentService();
            if($request->status == 'Head Agent'){
                $view= $agentService->viewBankAccountHead($id);
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Rekening Bank Ditampilkan',
                        'dataBankAccount' => [$view],
                    ];
            } else if($request->status == 'Agent'){
                $view= $agentService->viewBankAccount($id);
                        $statusCode = 200;
                            $response = [
                                'error' => false,
                                'message' => 'Rekening Bank Ditampilkan',
                                'dataBankAccount' => [$view],
                            ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Rekening Bank Belum Tercatat',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
        
}

public function createBankAccountAgency(Request $request, $id_agency)
    {
        try{
            if($request->status == "Head Agent"){
                // $borrowerAccount = Agents::where('id_head_agent',$id_agency)->first();
                $newBankAccount= new BankAccounts();
                $newBankAccount->id_head_agent = $id_agency;
                // $newBankAccount->phone_account_number = $request->phone_account_number;
                $newBankAccount->bank_account_number = $request->bank_account_number; 
                $newBankAccount->bank_account_name = $request->bank_account_name; 
                $newBankAccount->bank_name = $request->bank_name; 
                $newBankAccount->bank_branch = $request->bank_branch;
                $newBankAccount->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Bank Account Berhasil Ditambahkan',
                ];
            } else if($request->status == "Agent") {
                // $borrowerAccount = Agents::where('id_agent',$id_agency)->first();
                $newBankAccount= new BankAccounts();
                $newBankAccount->id_agent = $id_agency;
                // $newBankAccount->phone_account_number = $request->phone_account_number;
                $newBankAccount->bank_account_number = $request->bank_account_number; 
                $newBankAccount->bank_account_name = $request->bank_account_name; 
                $newBankAccount->bank_name = $request->bank_name; 
                $newBankAccount->bank_branch = $request->bank_branch;
                $newBankAccount->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Bank Account Berhasil Ditambahkan',
                ];                
            } else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Tersedia',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateBankAccountAgency(Request $request, $id_agency)
    {
        try{
            if($request->status == "Head Agent"){
                $bankAcc= BankAccounts::where('id_head_agent', $id_agency)
                        ->where('id',$request->id_bank)
                        ->first();
                $bankAcc->phone_account_number = $request->phone_account_number;
                $bankAcc->bank_account_number = $request->bank_account_number;
                $bankAcc->bank_account_name = $request->bank_account_name;
                $bankAcc->bank_name = $request->bank_name;
                $bankAcc->bank_branch = $request->bank_branch;
                $bankAcc->saveOrFail($request->all());
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Update Rekening Bank',
                    // 'data' => $bankAcc,
                ];
        
            } else if($request->status == "Agent"){
                $bankAcc= BankAccounts::where('id_agent', $id_agency)
                ->where('id',$request->id_bank)
                ->first();
                $bankAcc->phone_account_number = $request->phone_account_number;
                $bankAcc->bank_account_number = $request->bank_account_number;
                $bankAcc->bank_account_name = $request->bank_account_name;
                $bankAcc->bank_name = $request->bank_name;
                $bankAcc->bank_branch = $request->bank_branch;
                $bankAcc->saveOrFail($request->all());
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Update Rekening Bank',
                    // 'data' => $bankAcc,
                ];
            } else {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Tersedia',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    } 

    public function deleteBankAccountAgency(Request $request, $id_agency){
        try{
            if($request->status == "Head Agent"){
                $bankAcc= BankAccounts::where('id_head_agent', $id_agency)
                        ->where('id',$request->id_bank)
                        ->first();
                $bankAcc->delete();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Hapus Rekening Bank',
                    // 'data' => $bankAcc,
                ];
        
            } else if($request->status == "Agent"){
                $bankAcc= BankAccounts::where('id_agent', $id_agency)
                ->where('id',$request->id_bank)
                ->first();
                $bankAcc->delete();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Hapus Rekening Bank',
                    // 'data' => $bankAcc,
                ];
            } else {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Tersedia',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View all Borrowers of Agent for Head Agent
    public function viewBorrowerAgent(Request $request, $id)
    {
        try{
            $borrowerService = new BorrowerService();
            $borrowers= $borrowerService->viewBorrowerAgent($id);
            if(!$borrowers){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            }else{
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Data Peminjam',
                'dataBorrowerAgent' => $borrowers
            ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
     }

     public function updatePhotoProfile(Request $request, $id_agency)
     {
         try{
            if($request->status_agency == "Head Agent"){
                $updateHeadAgent= DetailAgents::where('id_head_agent', $id_agency)
                                ->first();
                if ($request->hasFile('profil_image')) {
                    if ($request->file('profil_image')->isValid()) {
                        $file_ext        = $request->file('profil_image')->getClientOriginalExtension();
                        $file_size       = $request->file('profil_image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = public_path() . $this->image_profile_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('profil_image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $id_profil_image_name = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('profil_image')->move($dest_path, $id_profil_image_name);
                            $updateHeadAgent->profil_image= $id_profil_image_name;
                        }
                    }
                }
                    $updateHeadAgent->saveOrFail();
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Berhasil foto profile',
                        
                    ];              
            } else if($request->status_agency == "Agent"){
                $updateAgent= DetailAgents::where('id_agent', $id_agency)
                                ->first();
                    if ($request->hasFile('profil_image')) {
                        if ($request->file('profil_image')->isValid()) {
                            $file_ext        = $request->file('profil_image')->getClientOriginalExtension();
                            $file_size       = $request->file('profil_image')->getClientSize();
                            $allow_file_exts = array('jpeg', 'jpg', 'png');
                            $max_file_size   = 1024 * 1024 * 10;
                            if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                                $dest_path     = public_path() . $this->image_profile_folder;
                                $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('profil_image')->getClientOriginalName());
                                $file_name     = str_replace(' ', '-', $file_name);
                                $id_profil_image_name = $file_name  . '.' . $file_ext;
            
                                // move file to serve directory
                                $request->file('profil_image')->move($dest_path, $id_profil_image_name);
                                $updateAgent->profil_image= $id_profil_image_name;
                            }
                        }
                    }
                        $updateAgent->saveOrFail();
                        $statusCode = 200;
                        $response = [
                            'error' => false,
                            'message' => 'Berhasil foto profile',
                            
                        ];                              
                } 
         }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        } 
     }
 

     public function detailBorrowerApplicant(Request $request, $id)
     {   
         try{
             if ($request->status == "Head Agent") {
                 $agentService= new AgentService();
                 $borrowerApplicant= $agentService->detailBorrowerApplicantHead($id,$request->id_loan); 
                 $statusCode = 200;
                 $response = [
                 'error' => false,
                 'message' => 'Daftar Calon Peminjam Belum Dikonfirmasi',
                 'dataBorrowers' => [$borrowerApplicant],
             ];
             } else if($request->status == "Agent"){
                 $agentService= new AgentService();
                 $borrowerApplicant= $agentService->detailBorrowerApplicant($id,$request->id_loan);
                 $statusCode = 200;
                 $response = [
                 'error' => false,
                 'message' => 'Daftar Calon Peminjam Belum Dikonfirmasi',
                 'dataBorrowers' => [$borrowerApplicant],
             ];
             } else {
                 $statusCode = 404;
                 $response = [
                         'error' => true,
                         'message' => 'Daftar Calon Peminjam Tidak Tersedia',
                 ];
             }
         } catch (Exception $ex){
             $statusCode = 404;
             $response = [
                 'error' => true,
                 'message' => 'Gagal Menampilkan Daftar Calon Peminjam',
             ];
         }
         finally {
             return response($response,$statusCode)->header('Content-Type','application/json');
         }
     }

     public function detailRegularLoan(Request $request, $id_agency)
     {
         try{            
             if($request->status == "Head Agent"){
                 $regulerLoan= new AgentService();
                 $detailRegulerLoan= $regulerLoan->detailRegulerLoanHead($id_agency, $request->id_loan);
                 $statusCode = 200;
                 $response = [
                     'error' => false,
                     'message' => 'Detail Peminjaman Reguler',
                     'dataBorrowerRegulerLoan' => [$detailRegulerLoan],
                 ];
             } else if($request->status == "Agent"){
                 $regulerLoan= new AgentService();
                 $detailRegulerLoan= $regulerLoan->detailRegulerLoan($id_agency, $request->id_loan);              
                 $statusCode = 200;
                 $response = [
                     'error' => false,
                     'message' => 'Detail Peminjaman Reguler',
                     'dataBorrowerRegulerLoan' =>[$detailRegulerLoan],
                 ];
             } else{
                 $statusCode = 404;
                 $response = [
                     'error' => true,
                     'message' => 'Data Tidak Ada',
                 ];
             }
         }catch (Exception $ex){
             $statusCode = 404;
             $response = [
                 'error' => true,
                 'message' => 'Gagal Menampilkan Daftar  Peminjam',
             ];
         }
         finally {
             return response($response,$statusCode)->header('Content-Type','application/json');
         }
     }
     

    public function withdraw(Request $request, $idAgency){
        try{
            if($request->status == "Head Agent"){
                $agentService= new AgentService();
                $commission= $agentService->withdrawHead($idAgency,  $request->new_balance, $request->withdraw);
                $statusCode = 200;
                $response = [
                     'error' => false,
                     'message' => 'Berhasil Tarik Dana',
                 ];
            } else if($request->status == "Agent"){
                $agentService= new AgentService();
                $commission= $agentService->withdraw($idAgency, $request->new_balance, $request->withdraw);
                $statusCode = 200;
                $response = [
                     'error' => false,
                     'message' => 'Berhasil Tarik Dana',
                 ];
            }
            
        }catch (Exception $ex){
             $statusCode = 404;
             $response = [
                 'error' => true,
                 'message' => 'Gagal Menampilkan Daftar  Peminjam',
             ];
         }
         finally {
             return response($response,$statusCode)->header('Content-Type','application/json');
         }
    }
    //  public function detailLateLoan(Request $request, $id_agency){
    //     try{            
    //         if($request->status == "Head Agent"){
    //             $regulerLoan= new AgentService();
    //             $detailRegulerLoan= $regulerLoan->detailLateLoanHead($id_agency, $request->id_loan);
    //             $statusCode = 200;
    //             $response = [
    //                 'error' => false,
    //                 'message' => 'Detail Peminjaman Reguler',
    //                 'dataBorrowerRegulerLoan' => [$detailRegulerLoan],
    //             ];
    //         } else if($request->status == "Agent"){
    //             $regulerLoan= new AgentService();
    //             $detailRegulerLoan= $regulerLoan->detailLateLoan($id_agency, $request->id_loan);              
    //             $statusCode = 200;
    //             $response = [
    //                 'error' => false,
    //                 'message' => 'Detail Peminjaman Reguler',
    //                 'dataBorrowerRegulerLoan' =>[$detailRegulerLoan],
    //             ];
    //         } else{
    //             $statusCode = 404;
    //             $response = [
    //                 'error' => true,
    //                 'message' => 'Data Tidak Ada',
    //             ];
    //         }
    //     }catch (Exception $ex){
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Gagal Menampilkan Daftar  Peminjam',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    //  }

    // Search Borrower that have loan by Head Agent
    //  public function searchBorrowerLocalIdHead(Request $request, $id)
    //  {
    //      try{
    //         $loans= Loans::where('id_head_agent', $id)->get();
    //         $search= Borrowers::with('detailBorrower')
    //         ->where('borrower_local_id', '=', $request->borrower_local_id)->first();
    //         if($loans->isEmpty()){
    //             $statusCode = 404;
    //             $response = [
    //             'error' => true,
    //             'message' => 'Data Tidak Ada',
    //         ];
    //         } elseif($search->isEmpty())
    //             {
    //                 $statusCode = 404;
    //                 $response = [
    //                 'error' => true,
    //                 'message' => 'Data Tidak Ada',
    //                 ];
    //             }
    //         else{
    //             $statusCode = 200;
    //             $response = [
    //             'error' => false,
    //             'message' => 'Hasil Pencarian Peminjam',
    //             'data' =>$search,
    //         ];
    //         }
    //      }catch (Exception $ex) {
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Gagal Tampilkan Data Peminjam',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    //  }


     // Search Borrower that have loan by an Agent
     public function searchBorrowerLocalIdAgent(Request $request, $id)
     {
         try{
            $borrowerService = new BorrowerService();
            $borrowers= $borrowerService->searchBorrowerAgent($id, $request->borrower_local_id);
            if(!$borrowers){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            } else{
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Hasil Pencarian Peminjam',
                'data' =>[$borrowers],
            ];
            }
         }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
     }

    //  list borrower untuk penambahan pinjaman
     public function listBorrowerAgency($id_sub_district)
    {
        try{
            $borrowerService = new BorrowerService();
            
            $list= $borrowerService->listBorrowerAgent($id_sub_district);
            if(!$list){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Peminjaman',
                    'dataListBorrower' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    } 

    //  list borrower untuk penambahan pinjaman
    public function detailBorrowerAgency($id_borrower)
    {
        try{
            $borrowerService = new BorrowerService();
            $detail= $borrowerService->detailBorrower($id_borrower);
            if(!$detail){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Peminjaman',
                    'dataDetailBorrower' => [$detail],
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    } 

    public function viewVillage(Request $request, $idSubdistrict){
        try {
            $list= Villages::where('id_sub_district', $idSubdistrict)->get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Kelurahan Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Kelurahan Ditampilkan',
                    'dataVillages' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Kelurahan',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewSubdistrics(Request $request, $idRegence){
        try {
            $list= SubDistricts::where('id_regency', $idRegence)->get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Kelurahan Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Kelurahan Ditampilkan',
                    'dataSubdistrics' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Kelurahan',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    public function viewRegency(Request $request, $idProvince){
        try {
            $list= Regencies::where('id_province', $idProvince)->get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Kota Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Kota Ditampilkan',
                    'dataRegencies' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Kota',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewProvince(Request $request)
    {
        try {
            $list= Provinces::get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Provinci Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Provinci Ditampilkan',
                    'dataProvinces' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Provinci',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

   
///////////////////////////////////////////////////////////////////////////

// public function createQuestion(Request $request, $id)
// {
//     try{
//         $agent = Agents::find($id);
//         if($agent->get()->isEmpty())
//         {   
//             $statusCode = 404;
//             $response = [
//                 'error' => false,
//                 'message' => 'Data Tidak Ditemukan',
//             ];  
//         } else{
//             $newQuestion = new Questions();
//             $newQuestion->id_agent = $agent->id;
//             $newQuestion->question = $request->question;
//             $newQuestion->save();
//             $statusCode = 200;
//             $response = [
//                 'error'=> false,
//                 'message' => 'Pertanyaan Ditambahkan',
//                 // 'Pertanyaan' => $newQuestion->question,
//             ];
//         }
//     } catch (Exception $ex) {
//         $statusCode = 404;
//         $response = [
//             'error' => true,
//             'message' => 'Gagal Tambahkan Pertanyaan',
//         ];
//     }
//     finally {
//         return response($response,$statusCode)->header('Content-Type','application/json');
//     }
// }
}