<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agents;
use App\Models\Borrowers;
use App\Models\HeadAgents;
use App\Models\Loans;
use App\Models\DetailBorrowers;
use App\Models\DetailAgents;
use App\Models\KospinLocation;
use App\Models\BankAccounts;
use App\Models\Questions;
use App\Models\Villages;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;
use App\Services\BorrowerService;

class AgentController extends Controller
{
    public $successStatus = 200;

    public function __construct(){
        $this->middleware('auth');
    }

    ///////////////Agent Profile//////////////////
    public function updatePersonalData(Request $request, $id)
    {
        try{
            $updateBorrower = Agents::find($id);

            if($updateBorrower->get()->isEmpty())
            {   
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                ];  
            } else{
                $updateDetailBorrower = DetailAgents::where('id_agent', $id)->first();                
                $updateDetailBorrower->id_sub_district = $request->id_sub_district;
                $updateDetailBorrower->address_local_id = $request->address_local_id;
                $updateDetailBorrower->saveOrFail($request->all());
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Update Data Pribadi'
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Data Pribadi',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewProfile (Request $request, $id)
    {
        try{
            $agent= DetailAgents::with('agent')->where('id_agent',$id)->first();
            if(!$agent){
                $statusCode = 404;
                $response = [
                'error' => false,
                'message' => 'Data Tidak Ditemukan',
            ];
            }else{
                $statusCode = 404;
                $response = [
                'error' => false,
                'message' => 'Data Pribadi Agent',
                'data' => $agent,
            ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal View Data Pribadi',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

   public function viewHistory(Request $request, $id)
    {
        try{
            $listHistory = AgentNotifications::
                where('id_agent',$id)
                ->where('topic', '=', $request->topic)
                ->get();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Tampilkan Riwayat Pinjaman',
                    'data' => $listHistory,
                ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Tampilan Riwayat Pinjaman Gagal',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewLoan(Request $request, $id)
    {   
        try{
            $loans= Loans::with('detailBorrower')->where('id_agent', $id)->get();        
            if($loans->isEmpty()){
                $statusCode = 404;
                $response = [
                        'error' => true,
                        'message' => 'Daftar Pinjaman Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Daftar Pinjaman Tersedia',
                'data' => $loans,
            ];
            }
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Daftar Pinjaman',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function createBankAccount(Request $request)
    {
        try{
            $borrowerAccount = Agents::find($request->id_agent)->first();
            if(!$borrowerAccount)
            {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Tersedia',
                ];
            } else{
                $newBankAccount= new BankAccounts();
                $newBankAccount->id_agent = $request->id_agent;
                $newBankAccount->phone_account_number = $request->phone_account_number;
                $newBankAccount->bank_account_number = $request->bank_account_number; 
                $newBankAccount->bank_account_name = $request->bank_account_name; 
                $newBankAccount->bank_name = $request->bank_name; 
                $newBankAccount->bank_branch = $request->bank_branch;
                $newBankAccount->saveOrFail($request->all());
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Bank Account Berhasil Ditambahkan',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    
    public function updateBankAccount(Request $request, $id)
    {
        try{
            $bankAcc= BankAccounts::where('id_agent',$id)
            ->where('bank_account_name','=',$request->bank_account_name)->first();
            if(!$bankAcc)
            {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Tersedia',
                ];
            } else {
                $bankAcc->phone_account_number = $request->phone_account_number;
                $bankAcc->bank_account_number = $request->bank_account_number;
                $bankAcc->bank_account_name = $request->new_bank_account_name;
                $bankAcc->bank_name = $request->bank_name;
                $bankAcc->bank_branch = $request->bank_branch;
                $bankAcc->saveOrFail($request->all());
                $statusCode = 200;
                $response = [
                    'error' => true,
                    'message' => 'Update Rekening Bank',
                    // 'data' => $bankAcc,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    } 

    // view borrower profile and loan status. borrower data was added by the agent
    public function viewBorrowerProfile(Request $request, $id)
    {
        try{
            // $loan= Loans::with('borrower')->with('detailBorrower')
            // ->where('id_agent',$id)
            // ->where('loan_type', '=', 'Ditambahkan Agen')
            // ->get();
            if(!$loan){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Peminjaman Didaftarkan Agen',
                    'data' => [$loan],
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    } 

    public function viewAllBorrowers(Request $request, $id)
    {
        try{
            $borrower= Loans::with('borrower')->with('detailBorrower')
                    ->where('id_agent',$id)->get();
            if(!$borrower)
            {
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
        }else{
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Daftar Peminjaman Milik Agent',
                'data' => $borrower,
            ];
        }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


     // public function viewCommissionBalance(Request $request, $id)
    // {
    //     try{
    //         $balance= DetailAgents::where('id_agent', $id)->get(['commission_balance']);                
    //         $statusCode = 200;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Saldo Ditampilkan',
    //             'data' => $balance,
    //         ];
        
    //     }catch (Exception $ex) {
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Gagal Menampilkan Saldo',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    // }


     // public function createQuestion(Request $request, $id)
    // {
    //     try{
    //         $agent = Agents::find($id);
    //         if($agent->get()->isEmpty())
    //         {   
    //             $statusCode = 404;
    //             $response = [
    //                 'error' => false,
    //                 'message' => 'Data Tidak Ditemukan',
    //             ];  
    //         } else{
    //             $newQuestion = new Questions();
    //             $newQuestion->id_agent = $agent->id;
    //             $newQuestion->question = $request->question;
    //             $newQuestion->save();
    //             $statusCode = 200;
    //             $response = [
    //                 'error'=> false,
    //                 'message' => 'Pertanyaan Ditambahkan',
    //                 // 'Pertanyaan' => $newQuestion->question,
    //             ];
    //         }
    //     } catch (Exception $ex) {
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Gagal Tambahkan Pertanyaan',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    // }
}